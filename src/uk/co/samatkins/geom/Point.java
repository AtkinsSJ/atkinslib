package uk.co.samatkins.geom;

import com.badlogic.gdx.math.Vector2;

public class Point implements Shape {
	
	private float x, y;

	public Point(float x, float y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public ShapeType getType() {
		return ShapeType.POINT;
	}

	@Override
	public Shape clone() {
		return new Point(x,y);
	}

	@Override
	public float getCentreX() {
		return x;
	}

	@Override
	public float getCentreY() {
		return y;
	}

	@Override
	public Vector2 getCentre() {
		return new Vector2(x,y);
	}

	@Override
	public float getLeft() {
		return x;
	}

	@Override
	public float getRight() {
		return x;
	}

	@Override
	public float getTop() {
		return y;
	}

	@Override
	public float getBottom() {
		return y;
	}

	@Override
	public float getWidth() {
		return 0;
	}

	@Override
	public float getHeight() {
		return 0;
	}

	@Override
	public boolean containsPoint(Vector2 point) {
		return false;
	}

	@Override
	public void setLeft(float left) {
		x = left;
	}

	@Override
	public void setRight(float right) {
		x = right;
	}

	@Override
	public void setTop(float top) {
		y = top;
	}

	@Override
	public void setBottom(float bottom) {
		y = bottom;
	}

	@Override
	public void setCentre(float x, float y) {
		this.x = x;
		this.y = y;
	}

}
