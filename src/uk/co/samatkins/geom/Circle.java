package uk.co.samatkins.geom;

import com.badlogic.gdx.math.Vector2;

public class Circle extends com.badlogic.gdx.math.Circle implements Shape {

	private static final long serialVersionUID = 4332150547937646637L;
	
	public Circle(float x, float y, float radius) {
		super(x,y,radius);
	}

	@Override
	public ShapeType getType() {
		return ShapeType.CIRCLE;
	}

	@Override
	public Shape clone() {
		return new Circle(x,y,radius);
	}

	@Override
	public float getCentreX() {
		return x;
	}

	@Override
	public float getCentreY() {
		return y;
	}

	@Override
	public Vector2 getCentre() {
		return new Vector2(x,y);
	}

	@Override
	public void setCentre(float x, float y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public float getLeft() {
		return x - radius;
	}

	@Override
	public float getRight() {
		return x + radius;
	}

	@Override
	public float getTop() {
		return y + radius;
	}

	@Override
	public float getBottom() {
		return y - radius;
	}

	@Override
	public boolean containsPoint(Vector2 point) {
		return this.contains(point);
	}

	@Override
	public float getWidth() {
		return radius + radius;
	}

	@Override
	public float getHeight() {
		return radius + radius;
	}

	@Override
	public void setLeft(float left) {
		x = left + radius;
	}

	@Override
	public void setRight(float right) {
		x = right - radius;
	}

	@Override
	public void setTop(float top) {
		y = top - radius;
	}

	@Override
	public void setBottom(float bottom) {
		y = bottom + radius;
	}

}
