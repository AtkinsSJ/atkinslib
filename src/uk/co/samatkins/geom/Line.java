package uk.co.samatkins.geom;

import com.badlogic.gdx.math.Vector2;

public class Line {
	
	public Vector2 start, end;

	public Line(Vector2 start, Vector2 end) {
		this.start = start;
		this.end = end;
	}
	
	public Line(float startX, float startY, float endX, float endY) {
		this(new Vector2(startX, startY), new Vector2(endX, endY));
	}
	
	public Vector2 calculateLeftNormal() {
		return end.cpy().sub(start).rotate(90).nor();
	}

}
