package uk.co.samatkins.geom;

import com.badlogic.gdx.math.Vector2;

/**
 * Extends Gdx's Rectangle with useful methods.
 * @author Sam
 *
 */
public class Rectangle extends com.badlogic.gdx.math.Rectangle
						implements Shape {

	private static final long serialVersionUID = -356010442366529991L;

	/**
	 * Create a rectangle from its bottom-left corner
	 * @param x Left
	 * @param y Bottom
	 * @param width Width
	 * @param height Height
	 */
	public Rectangle(float x, float y, float width, float height) {
		super(x, y, width, height);
	}

	@Override
	public ShapeType getType() {
		return ShapeType.RECTANGLE;
	}

	@Override
	public Shape clone() {
		return new Rectangle(x,y,width,height);
	}

	/* (non-Javadoc)
	 * @see uk.co.samatkins.geom.IShape#getCentreX()
	 */
	@Override
	public float getCentreX() {
		return x + (width/2f);
	}
	
	/* (non-Javadoc)
	 * @see uk.co.samatkins.geom.IShape#getCentreY()
	 */
	@Override
	public float getCentreY() {
		return y + (height/2f);
	}
	
	/* (non-Javadoc)
	 * @see uk.co.samatkins.geom.IShape#getCentre()
	 */
	@Override
	public Vector2 getCentre() {
		return new Vector2(getCentreX(), getCentreY());
	}
	
	@Override
	public void setCentre(float x, float y) {
		this.x = (x - width/2f);
		this.y = (y - height/2f);
	}
	
	/* (non-Javadoc)
	 * @see uk.co.samatkins.geom.IShape#getLeft()
	 */
	@Override
	public float getLeft() {
		return x;
	}
	
	/* (non-Javadoc)
	 * @see uk.co.samatkins.geom.IShape#getRight()
	 */
	@Override
	public float getRight() {
		return x+width;
	}
	
	/* (non-Javadoc)
	 * @see uk.co.samatkins.geom.IShape#getTop()
	 */
	@Override
	public float getTop() {
		return y+height;
	}

	/* (non-Javadoc)
	 * @see uk.co.samatkins.geom.IShape#getBottom()
	 */
	@Override
	public float getBottom() {
		return y;
	}

	@Override
	public boolean containsPoint(Vector2 point) {
		return this.contains(point.x, point.y);
	}

	@Override
	public void setLeft(float left) {
		this.setX(left);
	}

	@Override
	public void setRight(float right) {
		this.setX(right - width);
	}

	@Override
	public void setTop(float top) {
		this.setY(top - height);
	}

	@Override
	public void setBottom(float bottom) {
		this.setY(bottom);
	}
}
