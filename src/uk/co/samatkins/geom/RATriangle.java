package uk.co.samatkins.geom;

import com.badlogic.gdx.math.Vector2;

/**
 * Represents a Right-Angled Triangle.
 * @author Sam
 *
 */
public class RATriangle implements Shape {
	
	float x, y,
		width, height,
		halfWidth, halfHeight;
	boolean leftSolid, topSolid;

	public RATriangle(float left, float bottom, float width, float height,
			boolean leftSolid, boolean topSolid) {
		this.x = left;
		this.y = bottom;
		
		this.width = width;
		this.height = height;
		this.halfWidth = width/2f;
		this.halfHeight = height/2f;
		
		this.leftSolid = leftSolid;
		this.topSolid = topSolid;
	}

	@Override
	public ShapeType getType() {
		return ShapeType.RATRIANGLE;
	}

	@Override
	public Shape clone() {
		return new RATriangle(x,y,width,height,leftSolid,topSolid);
	}

	@Override
	public float getCentreX() {
		return x+halfWidth;
	}

	@Override
	public float getCentreY() {
		return y+halfHeight;
	}

	@Override
	public Vector2 getCentre() {
		return new Vector2(getCentreX(),getCentreY());
	}

	@Override
	public float getLeft() {
		return x;
	}

	@Override
	public float getRight() {
		return x + width;
	}

	@Override
	public float getTop() {
		return y + height;
	}

	@Override
	public float getBottom() {
		return y;
	}

	@Override
	public float getWidth() {
		return width;
	}

	@Override
	public float getHeight() {
		return height;
	}

	@Override
	public void setLeft(float left) {
		x = left;
	}

	@Override
	public void setRight(float right) {
		x = right - width;
	}

	@Override
	public void setTop(float top) {
		y = top - height;
	}

	@Override
	public void setBottom(float bottom) {
		y = bottom;
	}

	@Override
	public void setCentre(float x, float y) {
		this.x = x - halfWidth;
		this.y = y - halfHeight;
	}
	
	/**
	 * @return The angled side of this triangle as a Line,
	 * with the left side inwards.
	 */
	public Line getAngleLine() {
		if (leftSolid) {
			if (topSolid) {
				return new Line(getLeft(), getBottom(),
						getRight(), getTop());
			} else {
				return new Line(getRight(), getBottom(),
						getLeft(), getTop());
			}
		} else {
			if (topSolid) {
				return new Line(getLeft(), getTop(),
					getRight(), getBottom());
			} else {
				return new Line(getRight(), getTop(),
						getLeft(), getBottom());
			}
		}
	}

	@Override
	public boolean containsPoint(Vector2 point) {
		boolean inShape = point.x > getLeft()
						&& point.x < getRight()
						&& point.y > getBottom()
						&& point.y < getTop();
		
		if (!inShape) return false;
		
		Line line = getAngleLine();
		
		return ShapeCollider.isInLeftHalfSpace(line.start, line.end, point);
	}

}
