package uk.co.samatkins.geom;

import uk.co.samatkins.exceptions.NotImplementedException;
import uk.co.samatkins.geom.Shape.ShapeType;

import com.badlogic.gdx.math.Vector2;

/**
 * Static class for calculating intersections and collisions between Shapes.
 * @author Sam
 *
 * TODO: Make exitVector functions return 0 if there's no collision.
 * 		That way, we can just always apply the exit vector rather than 
 * 		running most of the collision code twice.
 */
public class ShapeCollider {

	/**
	 * Calculates whether the two shapes intersect.
	 * @param a
	 * @param b
	 * @return
	 */
	public static boolean doesIntersect(Shape a, Shape b) {
		switch (a.getType()) {
		case CIRCLE:
			return doesCircleIntersect((Circle)a, b);
		case RECTANGLE:
			return doesRectangleIntersect((Rectangle)a, b);
		case RATRIANGLE:
			return doesRATriangleIntersect((RATriangle)a, b);
			
		default:
			throw new RuntimeException("Unrecognized ShapeType: " + a.getType());
		}
	}

	private static boolean doesCircleIntersect(Circle a, Shape b) {
		switch (b.getType()) {
		case CIRCLE:
			return doesCircleIntersectCircle(a, (Circle) b);
		case RECTANGLE:
			return doesCircleIntersectRectangle(a, (Rectangle) b);
		case RATRIANGLE:
			return doesCircleIntersectRATriangle(a, (RATriangle) b);
		default:
			throw new RuntimeException("Unrecognized ShapeType: " + b.getType());
		}
	}

	private static boolean doesRectangleIntersect(Rectangle a, Shape b) {
		switch (b.getType()) {
		case CIRCLE:
			return doesCircleIntersectRectangle((Circle) b, a);
		case RECTANGLE:
			return doesRectangleIntersectRectangle(a, (Rectangle) b);
		case RATRIANGLE:
			return doesRectangleIntersectRATriangle(a, (RATriangle) b);
		default:
			throw new RuntimeException("Unrecognized ShapeType: " + b.getType());
		}
	}
	
	private static boolean doesRATriangleIntersect(RATriangle a, Shape b) {
		switch (b.getType()) {
		case CIRCLE:
			return doesCircleIntersectRATriangle((Circle) b, a);
		case RECTANGLE:
			return doesRectangleIntersectRATriangle((Rectangle) b, a);
		case RATRIANGLE:
			return doesRATriangleIntersectRATriangle(a, (RATriangle) b);
		default:
			throw new RuntimeException("Unrecognized ShapeType: " + b.getType());
		}
	}

	public static boolean doesCircleIntersectCircle(Circle a, Circle b) {
		return a.getCentre().dst(b.getCentre()) < a.radius + b.radius;
	}
	
	public static boolean doesRectangleIntersectRectangle(Rectangle a, Rectangle b) {
		return a.overlaps(b);
	}
	
	public static boolean doesCircleIntersectRectangle(Circle a, Rectangle b) {
		
		// Find closest point on rectangle to the circle
		Vector2 closestPointOnRectangle = a.getCentre();
		if (closestPointOnRectangle.x > b.getRight()) {
			closestPointOnRectangle.x = b.getRight();
		} else if (closestPointOnRectangle.x < b.getLeft()) {
			closestPointOnRectangle.x = b.getLeft();
		}
		
		if (closestPointOnRectangle.y > b.getTop()) {
			closestPointOnRectangle.y = b.getTop();
		} else if (closestPointOnRectangle.y < b.getBottom()) {
			closestPointOnRectangle.y = b.getBottom();
		}
		
		// Get distance between it and circle centre
		return (a.getCentre().sub(closestPointOnRectangle).len() < a.radius);
	}
	
	public static boolean doesCircleIntersectRATriangle(Circle circle, RATriangle tri) {
		// Check the left or right solid side of the triangle
		boolean inHor = tri.leftSolid
				? (circle.x > tri.getLeft())
				: (circle.x < tri.getRight());
		if (!inHor) {
			if (circle.y > tri.getTop()) {
				return circle.contains(
						tri.leftSolid ? tri.getLeft() : tri.getRight(),
						tri.getTop());
			} else if (circle.y < tri.getBottom()) {
				return circle.contains(
						tri.leftSolid ? tri.getLeft() : tri.getRight(),
						tri.getBottom());
			} else {
				if (tri.leftSolid){
					return (circle.x+circle.radius) > tri.getLeft(); 
				} else {
					return (circle.x-circle.radius) < tri.getRight(); 
				}
			}
		}
		
		// Check the top or bottom solid side of the triangle
		boolean inVer = tri.topSolid
				? (circle.y < tri.getTop())
				: (circle.y > tri.getBottom());
		if (!inVer) {
			if (circle.x > tri.getRight()) {
				return circle.contains(
						tri.getRight(),
						tri.topSolid ? tri.getTop() : tri.getBottom());
			} else if (circle.x < tri.getLeft()) {
				return circle.contains(
						tri.getLeft(),
						tri.topSolid ? tri.getTop() : tri.getBottom());
			} else {
				if (tri.topSolid){
					return (circle.y-circle.radius) < tri.getTop(); 
				} else {
					return (circle.y+circle.radius) > tri.getBottom(); 
				}
			}
		}
		
		// Check the diagonal side of the triangle
		Line diagonalLine = tri.getAngleLine();
		Vector2 circleCentre = circle.getCentre();
		boolean inDiagonal = isInLeftHalfSpace(
				diagonalLine.start, diagonalLine.end, circleCentre);
		
		if (!inDiagonal) {
			Vector2 away = new Vector2(diagonalLine.end).sub(diagonalLine.start);
			away.rotate(-90);
			Vector2 awayPos = away.cpy().add(diagonalLine.start);
			
			// Is the circle to the left of the right edge?
			if (isInLeftHalfSpace(diagonalLine.start, awayPos, circleCentre)) {
				
				awayPos.set(diagonalLine.end).add(away);
				
				// Is the circle to the right of the left edge?
				if (!isInLeftHalfSpace(diagonalLine.end, awayPos, circleCentre)) {
					awayPos.set(circleCentre).sub(diagonalLine.end);
					float component = component2d(awayPos, away);
					return component < circle.radius;
				} else {
					return circle.containsPoint(diagonalLine.start);
				}
				
			} else {
				return circle.containsPoint(diagonalLine.end);
			}
		}
		
		// Between all three sides - return true!
		return true;
	}
	
	public static boolean doesRectangleIntersectRATriangle(Rectangle a,
			RATriangle b) {
		throw new NotImplementedException("RATriangle -> Rectangle collision not yet implemented.");
	}
	
	public static boolean doesRATriangleIntersectRATriangle(RATriangle a, RATriangle b) {
		throw new NotImplementedException("RATriangle -> RATriangle collision not yet implemented.");
	}

	/**
	 * Calculate the movement required for a, for it to no longer collide with b.
	 * @param a
	 * @param b
	 * @return
	 */
	public static Vector2 calculateExitVector(Shape a, Shape b) {
		if (a.getType() == ShapeType.CIRCLE){
			if (b.getType() == ShapeType.CIRCLE) {
				return calculateExitVectorCC((Circle)a, (Circle)b);
			} else if (b.getType() == ShapeType.RECTANGLE) {
				return calculateExitVectorCR((Circle)a, (Rectangle)b);
			} else if (b.getType() == ShapeType.RATRIANGLE) {
				return calculateExitVectorCRat((Circle)a, (RATriangle)b);
			}
		} else if (a.getType() == ShapeType.RECTANGLE) {
			if (b.getType() == ShapeType.CIRCLE) {
				return calculateExitVectorCR((Circle)b, (Rectangle)a).mul(-1);
			} else if (b.getType() == ShapeType.RECTANGLE){
				return calculateExitVectorRR((Rectangle)a, (Rectangle)b);
			} else if (b.getType() == ShapeType.RATRIANGLE){
				return calculateExitVectorRRat((Rectangle)a, (RATriangle)b);
			}
		}
		
		throw new NotImplementedException("calculateExitVector() for " + a.getType()
				+ "/" + b.getType() + " is not yet implemented.");
	}
	
	public static Vector2 calculateExitVectorCC(Circle a, Circle b) {
		// Move directly away
		Vector2 direction = new Vector2(b.x-a.x, b.y-a.y);
		float distance = direction.len();
		direction.nor().mul(distance - a.radius - b.radius);
		
		return direction;
	}

	public static Vector2 calculateExitVectorRR(Rectangle a, Rectangle b) {
		// Calculate the overlap vector
		Vector2 vector = b.getCentre().sub(a.getCentre());
		
		float xChange = (a.width + b.width) / 2f,
			  yChange = (a.height + b.height) / 2f;
		vector.x += (vector.x > 0 ? - xChange : xChange);
		vector.y += (vector.y > 0 ? - yChange : yChange);
		
		// Zero-out the longest side
		// TODO: Fix odd behaviour when clipping corners.
		if (Math.abs(vector.x) > Math.abs(vector.y)) {
			vector.x = 0;
		} else {
			vector.y = 0;
		}
		
		return vector;
	}
	
	public static Vector2 calculateExitVectorCR(Circle a, Rectangle b) {
		// Find closest point on rectangle to the circle
		Vector2 closestPointOnRectangle = a.getCentre();
		if (closestPointOnRectangle.x >= b.getRight()) {
			closestPointOnRectangle.x = b.getRight();
		} else if (closestPointOnRectangle.x <= b.getLeft()) {
			closestPointOnRectangle.x = b.getLeft();
		}
		
		if (closestPointOnRectangle.y >= b.getTop()) {
			closestPointOnRectangle.y = b.getTop();
		} else if (closestPointOnRectangle.y <= b.getBottom()) {
			closestPointOnRectangle.y = b.getBottom();
		}
		
		// Get distance between it and circle centre
		Vector2 exitVector = a.getCentre().sub(closestPointOnRectangle);
		if (exitVector.len2() < 0.001f) {
			/* When the circle centre is on the edge of the rect, we need
			 * to create the vector from another position.
			 * The rectangle's centre is 'good enough' - though it may
			 * move the circle at a funny angle, it's incredibly rare.
			 */
			exitVector = a.getCentre().sub(b.getCentre());
		}
		float overlap = a.radius - a.getCentre().dst(closestPointOnRectangle);
		
		exitVector.nor().mul(overlap);
		
		return exitVector;
	}
	
	public static Vector2 calculateExitVectorRRat(Rectangle a, RATriangle b) {
		throw new NotImplementedException("calculateVectorRRat not implemented.");
	}
	
	public static Vector2 calculateExitVectorCRat(Circle circle, RATriangle tri) {
		// Apologies: This is a monster of a function.
		// Let's just hope it works and I never have to mess with it again.
		
		Vector2 circleCentre = circle.getCentre();
		
		// Check the left or right solid side of the triangle
		boolean inHor = tri.leftSolid
				? (circle.x > tri.getLeft())
				: (circle.x < tri.getRight());
		if (!inHor) {
			if (circle.y > tri.getTop()) {
				if (circle.contains(
						tri.leftSolid ? tri.getLeft() : tri.getRight(),
						tri.getTop())) {
					return calculateExitVectorCirclePoint(circle,
							new Vector2(tri.leftSolid ? tri.getLeft() : tri.getRight(),
									tri.getTop()));
				} else {
					return Vector2.Zero;
				}
			} else if (circle.y < tri.getBottom()) {
				if (circle.contains(
						tri.leftSolid ? tri.getLeft() : tri.getRight(),
						tri.getBottom())) {
					return calculateExitVectorCirclePoint(circle,
							new Vector2(tri.leftSolid ? tri.getLeft() : tri.getRight(),
									tri.getBottom()));
				} else {
					return Vector2.Zero;
				}
			} else {
				if (tri.leftSolid){
					return new Vector2(tri.getLeft() - (circle.x+circle.radius), 0);
				} else {
					return new Vector2(tri.getRight() - (circle.x-circle.radius), 0);
				}
			}
		}
		
		// Check the top or bottom solid side of the triangle
		boolean inVer = tri.topSolid
				? (circle.y < tri.getTop())
				: (circle.y > tri.getBottom());
		if (!inVer) {
			if (circle.x > tri.getRight()) {
				if (circle.contains(
						tri.getRight(),
						tri.topSolid ? tri.getTop() : tri.getBottom())) {
					return calculateExitVectorCirclePoint(circle,
							new Vector2(tri.getRight(),
									tri.topSolid ? tri.getTop() : tri.getBottom()));
				} else {
					return Vector2.Zero;
				}
			} else if (circle.x < tri.getLeft()) {
				if (circle.contains(
						tri.getLeft(),
						tri.topSolid ? tri.getTop() : tri.getBottom())) {
					return calculateExitVectorCirclePoint(circle,
							new Vector2(tri.getLeft(),
									tri.topSolid ? tri.getTop() : tri.getBottom()));
				} else {
					return Vector2.Zero;
				}
			} else {
				if (tri.topSolid){
					return new Vector2(0, tri.getTop() - (circle.y-circle.radius));
				} else {
					return new Vector2(0, tri.getBottom() - (circle.y+circle.radius));
				}
			}
		}
		
		// Check the diagonal side of the triangle
		Line diagonalLine = tri.getAngleLine();
		boolean inDiagonal = isInLeftHalfSpace(
				diagonalLine.start, diagonalLine.end, circleCentre);
		
		if (!inDiagonal) {
			Vector2 away = new Vector2(diagonalLine.end).sub(diagonalLine.start);
			away.rotate(-90);
			Vector2 awayPos = away.cpy().add(diagonalLine.start);
			
			// Is the circle to the left of the right edge?
			if (isInLeftHalfSpace(diagonalLine.start, awayPos, circleCentre)) {
				
				awayPos.set(diagonalLine.end).add(away);
				
				// Is the circle to the right of the left edge?
				if (!isInLeftHalfSpace(diagonalLine.end, awayPos, circleCentre)) {
					awayPos.set(circleCentre).sub(diagonalLine.end);
					float distance = circle.radius - component2d(awayPos, away);
					return away.nor().mul(distance);
				} else {
					return calculateExitVectorCirclePoint(circle,
							diagonalLine.start);
				}
				
			} else {
				return calculateExitVectorCirclePoint(circle,
						diagonalLine.end);
			}
		}
		
		// Between all three sides - oh dear.
		
		// Get distances from the three sides.
		float hDistance = tri.leftSolid ? circleCentre.x - tri.getLeft()
										: tri.getRight() - circleCentre.x;
		float vDistance = tri.topSolid ? tri.getTop() - circleCentre.y
										: circleCentre.y - tri.getBottom();
		
		Vector2 cornerToCircle = circleCentre.cpy().sub(diagonalLine.start);
		Vector2 leftNormal = diagonalLine.calculateLeftNormal();
		float diagDistance = component2d(cornerToCircle, leftNormal);
		
		if (hDistance < vDistance && hDistance < diagDistance) {
			// h smallest
			return new Vector2(hDistance + circle.radius * (tri.leftSolid ? -1 : 1), 0);
		} else if (vDistance < diagDistance) {
			// v smallest
			return new Vector2(0, vDistance + circle.radius * (tri.topSolid ? 1 : -1));
		} else {
			// diag smallest
			return leftNormal.mul(- diagDistance - circle.radius);
		}
	}
	
	public static Vector2 calculateExitVectorRatRat(RATriangle a, RATriangle b) {
		throw new NotImplementedException("calculateVectorRatRat not implemented.");
	}
	
	public static Vector2 calculateExitVectorCirclePoint(Circle circle, Vector2 point) {
		Vector2 movement = point.sub(circle.getCentre());
		float length = movement.len();
		return movement.nor().mul(length - circle.radius);
	}
	
	/**
	 * Is 'point' on the left of the line from 'start' to 'end'?
	 * @param start
	 * @param end
	 * @param point
	 * @return
	 */
	public static boolean isInLeftHalfSpace(Vector2 start, Vector2 end, Vector2 point) {
		
		Vector2 line = end.cpy().sub(start);
		Vector2 v = point.cpy().sub(start);
		line.rotate(-90);
		
		return line.dot(v) <= 0;
	}
	
	/**
	 * http://ubuntu-gamedev.org/2D%20Collision%20Detection.html
	 * From what I understand, this returns the length of a vector in a given direction.
	 * @param vector
	 * @param direction
	 * @return
	 */
	public static float component2d(Vector2 vector, Vector2 direction) {
		return vector.dot(direction.cpy().nor());
	}
}
