package uk.co.samatkins.geom;

import com.badlogic.gdx.math.Vector2;

public interface Shape {
	
	public enum ShapeType {
		RECTANGLE,
		CIRCLE,
		POINT,
		RATRIANGLE;
	}
	
	public ShapeType getType();

	public Shape clone();

	/**
	 * @return The x position of the centre of this shape.
	 */
	public float getCentreX();
	
	/**
	 * @return The y position of the centre of this shape.
	 */
	public float getCentreY();
	
	/**
	 * @return The position of the centre of this shape.
	 */
	public Vector2 getCentre();
	
	/**
	 * @return The left-most x position of this shape.
	 */
	public float getLeft();

	/**
	 * @return The right-most x position of this shape.
	 */
	public float getRight();

	/**
	 * @return The highest y position of this shape.
	 */
	public float getTop();

	/**
	 * @return The lowest y position of this shape.
	 */
	public float getBottom();
	
	/**
	 * @return The largest horizontal distance in the shape.
	 */
	public float getWidth();
	
	/**
	 * @return The largest vertical distance in the shape.
	 */
	public float getHeight();

	/**
	 * Does this shape contain the given point?
	 * @param point
	 * @return
	 */
	public boolean containsPoint(Vector2 point);
	
	public void setLeft(float left);
	public void setRight(float right);
	public void setTop(float top);
	public void setBottom(float bottom);

	public void setCentre(float x, float y);

}