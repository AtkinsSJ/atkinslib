package uk.co.samatkins;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import uk.co.samatkins.components.IComponent;
import uk.co.samatkins.components.geom.PositionComponent;
import uk.co.samatkins.geom.Shape;
import uk.co.samatkins.geom.ShapeCollider;

import com.badlogic.gdx.math.Vector2;

public class EntityFilter {
	
	private Collection<Entity> entities;

	public EntityFilter(Collection<Entity> entities) {
		this.entities = new HashSet<Entity>(entities);
	}
	
	public Collection<Entity> getEntities() {
		return entities;
	}
	
	public int size() {
		return entities.size();
	}
	
	public Entity first() {
		return size() > 0 ? entities.iterator().next() : null;
	}

	/**
	 * Discard entities that do not overlap the given position
	 * @param position
	 * @return The EntityFilter for chaining
	 */
	public EntityFilter overlappingPosition(Vector2 position) {
		PositionComponent pos;
		
		List<Entity> keep = new ArrayList<Entity>();
		
		for (Entity entity: entities) {
			pos = entity.getComponent(PositionComponent.class);
			if (pos == null) continue;
			
			// Check shape
			if (pos.getShape().containsPoint(position)) {
				keep.add(entity);
			}
		}
		
		entities.retainAll(keep);
		
		return this; // Chain
	}
	
	/**
	 * Discard entities that do not overlap the given Shape
	 * @param shape
	 * @return The EntityFilter for chaining.
	 */
	public EntityFilter overlappingShape(Shape shape) {
		
		PositionComponent pc;
		
		List<Entity> keep = new ArrayList<Entity>();
		
		for (Entity entity: entities) {
			pc = entity.getComponent(PositionComponent.class);
			if (pc == null) continue;
			
			// Check shape
			if (ShapeCollider.doesIntersect(shape, pc.getShape())) {
				keep.add(entity);
			}
		}
		
		entities.retainAll(keep);
		
		return this;
	}
	
	/**
	 * Discard entities that do not overlap the given Entity
	 * @param entity
	 * @return The EntityFilter for chaining.
	 */
	public EntityFilter overlappingEntity(Entity entity) {
		PositionComponent pc = entity.getComponent(PositionComponent.class);
		if (pc != null) {
			return overlappingShape(pc.getShape());
		} else {
			throw new RuntimeException("Entity without position supplied to overlappingEntity()");
		}
	}

	/**
	 * Discard entities that do not have the given tag.
	 * @param tag
	 * @return The EntityFilter for chaining
	 */
	public EntityFilter withTag(String tag) {
		List<Entity> keep = new ArrayList<Entity>();
		
		for (Entity entity: entities) {
			if (entity.hasTag(tag)) {
				keep.add(entity);
			}
		}
		
		entities.retainAll(keep);
		
		return this;
	}
	
	/**
	 * Discard entities that have the given tag.
	 * @param tag
	 * @return The EntityFilter for chaining
	 */
	public EntityFilter withoutTag(String tag) {
		List<Entity> remove = new ArrayList<Entity>();
		
		for (Entity entity: entities) {
			if (entity.hasTag(tag)) {
				remove.add(entity);
			}
		}
		
		entities.removeAll(remove);
		
		return this;
	}

	/**
	 * Discard entities that don't have the given component
	 * @param componentClass
	 * @return
	 */
	public EntityFilter withComponent(Class<? extends IComponent> componentClass) {
		
		List<Entity> keep = new ArrayList<Entity>();
		
		for (Entity entity: entities) {
			if (entity.getComponent(componentClass) != null) {
				keep.add(entity);
			}
		}
		
		entities.retainAll(keep);
		
		return this;
	}
}
