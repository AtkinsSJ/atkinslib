package uk.co.samatkins;

import com.badlogic.gdx.tools.imagepacker.TexturePacker2;

public class Packer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TexturePacker2.process(args[0]+"-android/assets/unpacked",
        		args[0]+"-android/assets", "packed");
	}

}
