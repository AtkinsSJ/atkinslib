package uk.co.samatkins;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool;

/**
 * A texture region with a position and z-index, added with {@link #drawSprite},
 * and then rendered in {@link #draw}
 * 
 * NB: In many ways this is a repetition of GDX's own Sprite class.
 * HOWEVER, actually using that means testing won't work.
 * 
 * @author Sam
 *
 */
public class Sprite implements Comparable<Sprite>, Pool.Poolable {
	public TextureRegion textureRegion;
	public float x, y;
	public float w,h;
	public int zIndex;
	public Color color;
	public float rotation;
	public float scale;
	
	public Sprite() {
		textureRegion = null;
		x = y = 0;
		w = h = 0;
		zIndex = 0;
		color = Color.WHITE;
		rotation = 0;
		scale = 1;
	}
	
	public void set(TextureRegion tr, float x, float y, int zIndex,
			Color color, float rotation, float scale) {
		this.textureRegion = tr;
		this.x = x;
		this.y = y;
		this.w = tr.getRegionWidth();
		this.h = tr.getRegionHeight();
		this.zIndex = zIndex;
		this.color = color;
		this.rotation = rotation;
		this.scale = scale;
	}

	@Override
	public int compareTo(Sprite o) {
		return this.zIndex - o.zIndex;
	}

	@Override
	public void reset() {
		textureRegion = null;
		x = y = 0;
		w = h = 0;
		zIndex = 0;
		color = Color.WHITE;
		rotation = 0;
		scale = 1;
	}
}