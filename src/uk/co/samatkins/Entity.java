package uk.co.samatkins;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.badlogic.gdx.utils.JsonValue;
import uk.co.samatkins.components.IComponent;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.OrderedMap;


public class Entity implements Json.Serializable {
	
	private Scene<?> scene;
	private Map<Class<IComponent>, IComponent> components;
	
	private Set<String> tags;
	
	private int index; // Serialised entity id
	
	public Entity(String tag) {
		components = new HashMap<Class<IComponent>, IComponent>();
		tags = new HashSet<String>();
		tags.add(tag);
	}

	public Entity() {
		this(null);
	}
	
	public void setScene(Scene<?> scene) {
		this.scene = scene;
	}
	
	public Scene<?> getScene() {
		return scene;
	}
	
	/**
	 * Called after this entity has been added to a scene.
	 */
	public void added() {
		for (IComponent c: components.values()) {
			c.addedToScene(getScene());
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T extends IComponent> T addComponent(T component) {
		components.put((Class<IComponent>) component.getClass(), component);
		component.added(this);
		return component;
	}
	public int getComponentCount() {
		return components.size();
	}
	
	public void update(float delta) {
		for (IComponent component: components.values()) {
			component.update(delta);
		}
		for (IComponent component: components.values()) {
			component.lateUpdate(delta);
		}
		for (IComponent component: components.values()) {
			component.render(delta);
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends IComponent> T getComponent(Class<T> componentClass) {
		return (T) components.get(componentClass);
	}
	
	@Deprecated
	public String getType() {
		return null;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString())
			.append(", type ").append(getType())
			.append(", with ").append(getComponentCount()).append(" components: [");
		
		for (IComponent component: this.components.values()) {
			sb.append(component.toString()).append(" ");
		}
		sb.append("]");
		
		return sb.toString();
	}
	
	public void addTag(String tag) {
		tags.add(tag);
		if (scene != null) {
			scene.addTagToEntity(this, tag);
		}
	}
	
	public void addTags(String ... tags) {
		for (String tag : tags) {
			addTag(tag);
		}
	}
	
	public void removeTag(String tag) {
		this.tags.remove(tag);
		if (scene != null) {
			scene.removeTagFromEntity(this, tag);
		}
	}
	
	public boolean hasTag(String tag) {
		return tags.contains(tag);
	}

	public Collection<String> getTags() {
		return tags;
	}

	public void removed() {
		for (IComponent c: components.values()) {
			c.removedFromScene(getScene());
		}
		scene = null;
	}

	public void setIndex(int i) {
		this.index = i;
	}

	@Override
	public void write(Json json) {
		json.writeValue("tags", tags.toArray());
		json.writeValue("components", components.values().toArray());
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		// TODO Auto-generated method stub
		
	}
}
