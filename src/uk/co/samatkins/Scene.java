package uk.co.samatkins;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import uk.co.samatkins.components.geom.ColliderGridComponent;
import uk.co.samatkins.components.geom.PositionComponent;
import uk.co.samatkins.geom.Circle;
import uk.co.samatkins.geom.Shape;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.ReflectionPool;

public class Scene<G extends Game> extends Stage
									implements Screen {
	
	protected G game;
	private Set<Entity> entitiesToAdd;
	private Set<Entity> entitiesToRemove;
	private Set<Entity> entities;
	
	private Map<String, List<Entity>> entityTags;
	
	private Pool<Sprite> spritePool;
	private SortedMap<Integer, List<Sprite>> sprites;
	
	private Rectangle cameraBounds;
	private boolean cameraBounded;
	private float scale;
	
	private Map<String, Sound> sounds;
	
	private boolean debugMode = false;
	private ShapeRenderer debugShapeRenderer;
	private FPSLogger fpsLogger;
	
	public interface ResizeListener {
		/**
		 * Called whenever the simulated screen size changes.
		 * @param width - Scene width (screen width with scaling applied)
		 * @param height - Scene height (screen height with scaling applied)
		 */
		public void resize(int width, int height);
	}
	private List<ResizeListener> resizeListeners;

	public Scene(G game) {
		this(game, new SpriteBatch());
	}
	
	public Scene(G game, SpriteBatch spriteBatch) {
		super( 0,0, false, spriteBatch);
		this.game = game;
		
		this.entitiesToAdd = new HashSet<Entity>();
		this.entitiesToRemove = new HashSet<Entity>();
		this.entities = new HashSet<Entity>();
		this.entityTags = new HashMap<String, List<Entity>>();
		
		this.sprites = new TreeMap<Integer, List<Sprite>>();
		this.spritePool = new ReflectionPool<Sprite>(Sprite.class);
		
		this.cameraBounds = new Rectangle();
		this.cameraBounded = false;
		this.scale = 1;
		
		this.sounds = new HashMap<String, Sound>();
		this.loadSounds();
		this.resizeListeners = new ArrayList<Scene.ResizeListener>();
	}
	
	/**
	 * Called whenever sound files need to be loaded.
	 * Override and specify sounds to load, using {@link #loadSound(String, String)}
	 */
	public void loadSounds() {
		
	}
	
	/**
	 * Create a Sound from the filename, and store it under the name, for
	 * later playing with {@link #playSound(String)}
	 * @param name
	 * @param filename
	 */
	protected void loadSound(String name, String filename) {
		this.sounds.put(name, Gdx.audio.newSound( Gdx.files.internal(filename) ) );
	}
	
	/**
	 * Play a sound and return the instance id, as in {@link Sound#play()}
	 * @param name
	 * @return
	 * @throws Exception If sound does not exist.
	 */
	public long playSound(String name) {
		/* TODO: Create a positional sound component, that adjusts panning 
		 * and volume based on the Entity position relative to the camera.
		 */
		if (this.sounds.containsKey(name)) {
			return this.sounds.get(name).play();
		} else {
			Gdx.app.error("Sound", "Trying to play non-existent sound: " + name);
			return -1;
		}
	}
	
	/**
	 * Add the entity to the scene, and return it.
	 * @param e
	 * @return The entity that was given
	 */
	public Entity addEntity(Entity e) {
		entitiesToAdd.add(e);
		for (String tag: e.getTags()) {
			addTagToEntity(e, tag);
		}
		e.setScene(this);
		e.added();
		return e;
	}
	
	public void removeEntity(Entity e) {
		entitiesToRemove.add(e);
	}
	
	/**
	 * Adds the entity to the array for its tag.
	 * If the tag is null, do nothing.
	 * If the tag doesn't exist, create it.
	 * @param e
	 * @param tag
	 */
	public void addTagToEntity(Entity e, String tag) {
		if (tag == null) return;
		
		if (!entityTags.containsKey(tag)) {
			entityTags.put(tag, new ArrayList<Entity>());
		}
		entityTags.get(tag).add(e);
	}
	
	/**
	 * Remove the entity from the array for the given tag.
	 * If the tag is null, or the tag array doesn't exist, does nothing.
	 * @param e
	 * @param tag
	 */
	public void removeTagFromEntity(Entity e, String tag) {
		if (tag == null) return;
		
		if (entityTags.containsKey(tag)) {
			entityTags.get(tag).remove(e);
		}
	}

	/**
	 * @return The number of entities that currently exist in the scene.
	 */
	public int getEntityCount() {
		return entities.size();
	}

	@Override
	public void dispose() {
		// Dispose of sounds
		for (Sound sound: this.sounds.values()) {
			sound.dispose();
		}
		this.sounds.clear();
	}
	
	@Override
	public void show() {
		if (Gdx.input != null) {
			Gdx.input.setInputProcessor(this);
		}
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {
	}
	
	@Override
	public void resume() {

	}

	@Override
	public void render(float delta) {
		update(delta);
		draw();
		
		if (debugMode) {
			fpsLogger.log();
		}
	}
	
	/**
	 * Save the current state to the given file
	 * @param fileHandle
	 */
	public void save(FileHandle fileHandle) {
//		Writer writer = null;
//
//		Gdx.app.debug("Saving Scene", "Attempting to save scene state to " + fileHandle.file().getAbsolutePath());
//
//		try {
//			fileHandle.file().createNewFile();
//			writer = fileHandle.writer(false);
//			Json json = new Json();
//
//			// Create flat array from entities
//			Entity[] entityArray = this.entities.toArray(new Entity[]{});
//
//			// Assign entity IDs
//			for (int i=0; i<entityArray.length; i++) entityArray[i].setIndex(i);
//
//			writer.write(json.prettyPrint(entityArray));
//
//		} catch (IOException e) {
//			// TODO Handle saving failure gracefully
//			e.printStackTrace();
//		} finally {
//			try {
//				writer.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
	}
	
	/**
	 * Load the state from the given file
	 * @param fileHandle
	 */
//	@SuppressWarnings("unchecked")
	public void load(FileHandle fileHandle) {
//		try {
//			ObjectInputStream in = new ObjectInputStream(fileHandle.read());
//			this.entities = (Set<Entity>) in.readObject();
//			
//			for (Entity entity: this.entities) {
//				entity.setScene(this);
//				
//				// Rebuild tag tables
//				for (String tag: entity.getTags()) {
//					addTagToEntity(entity, tag);
//				}
//			}
//			
//		} catch (IOException e) {
//			// TODO Handle loading failure gracefully
//			e.printStackTrace();
//		} catch (ClassNotFoundException e) {
//			// TODO Handle loading failure gracefully
//			e.printStackTrace();
//		}
	}
	
	/**
	 * Update all entities
	 * @param delta
	 */
	public void update(float delta) {
		
		// Properly add any new entities
		if (!entitiesToAdd.isEmpty()) {
			entities.addAll(entitiesToAdd);
			
			entitiesToAdd.clear();
		}
		
		// Remove any entities flagged for removal
		if (!entitiesToRemove.isEmpty()) {
			// Also have to remove them from entityTypes
			for (List<Entity> list: this.entityTags.values()) {
				list.removeAll(entitiesToRemove);
			}
			
			// Call destroy methods
			for (Entity e: entitiesToRemove) {
				e.removed();
			}
			
			entities.removeAll(entitiesToRemove);
			
			entitiesToRemove.clear();
		}
		
		// Update entities
		for (Entity e: entities) {
			e.update(delta);
		}
		
		// Update the UI.
		if (Gdx.app != null) {
			this.act(delta);
		} else {
			this.getRoot().act(delta);
		}
	}
	
	/**
	 * Draw all sprites
	 */
	public void draw() {
		// spritebatch being null is a signal that rendering is disabled
		if (getSpriteBatch() != null) {

			getCamera().update();
			
			SpriteBatch spriteBatch = getSpriteBatch();
			
			Gdx.gl.glClearColor(0, 0, 0, 1);
			Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
			
			spriteBatch.setProjectionMatrix(getCamera().combined);
			
			spriteBatch.begin();
			
			List<Sprite> spriteList = null;
			for (Integer zIndex: sprites.keySet()) {
				spriteList = sprites.get(zIndex);
				
				for (Sprite sprite: spriteList) {
					spriteBatch.setColor(sprite.color);
					if (sprite.rotation == 0) {
						spriteBatch.draw( sprite.textureRegion,
								sprite.x, sprite.y );
					} else {
						spriteBatch.draw( sprite.textureRegion,
								sprite.x, sprite.y,
								sprite.w/2, sprite.h/2, // rotation origin
								sprite.w, sprite.h,
								sprite.scale, sprite.scale,
								sprite.rotation);
					}
					
					spritePool.free(sprite);
				}
				
				spriteList.clear();
			}
			
			this.getRoot().draw(spriteBatch, 1);
			
			spriteBatch.end();
			
			if (debugMode) {
				drawDebug(debugShapeRenderer);
			}
		}
		
//		clearSprites();
	}
	
	public void drawDebug(ShapeRenderer shapeRenderer) {
		shapeRenderer.setProjectionMatrix(getCamera().combined);
		
		Shape shape;
		PositionComponent positionC;
		ColliderGridComponent colliderC;
		
		for (Entity e: entities) {
			positionC = e.getComponent(PositionComponent.class);
			if (positionC != null) {

				shapeRenderer.setColor(1, 1, 1, 1);
				shape = positionC.getShape();
				switch (shape.getType()) {
				case CIRCLE:
					shapeRenderer.begin(ShapeType.Line);
					shapeRenderer.circle(shape.getCentreX(), shape.getCentreY(), ((Circle)shape).radius);
					break;
				default:
					shapeRenderer.begin(ShapeType.Line);
					shapeRenderer.rect(
							shape.getLeft(),
							shape.getBottom(),
							shape.getWidth(),
							shape.getHeight());
					break;
				
				}
				shapeRenderer.end();
				
				colliderC = e.getComponent(ColliderGridComponent.class);
				if (colliderC != null) {
					colliderC.debugRender(shapeRenderer);
				}
			}
		}
	}
	
	/**
	 * Add a TextureRegion to the draw queue.
	 * @param textureRegion
	 * @param x
	 * @param y
	 * @param zIndex - The higher this is, the later the sprite will be drawn.
	 */
	public void drawSprite(TextureRegion textureRegion, float x, float y, int zIndex) {
		drawSprite(textureRegion, x, y, zIndex, Color.WHITE);
	}
	
	/**
	 * Add a TextureRegion to the draw queue.
	 * @param textureRegion
	 * @param x
	 * @param y
	 * @param zIndex - The higher this is, the later the sprite will be drawn.
	 * @param color - Color to tint the TextureRegion with
	 */
	public void drawSprite(TextureRegion textureRegion, float x, float y, int zIndex, Color color) {
		drawSprite(textureRegion, x, y, zIndex, color, 0, 1);
	}
	
	/**
	 * Add a TextureRegion to the draw queue.
	 * @param textureRegion
	 * @param x
	 * @param y
	 * @param zIndex - The higher this is, the later the sprite will be drawn.
	 * @param color - Color to tint the TextureRegion with
	 * @param rotation - The rotation of the sprite
	 * @param scale 
	 */
	public void drawSprite(TextureRegion textureRegion, float x, float y,
			int zIndex, Color color, float rotation, float scale) {
		Sprite s = spritePool.obtain();
		s.set(textureRegion, x, y, zIndex, color, rotation, scale);
		
		// Insert the sprite
		List<Sprite> list = sprites.get(zIndex);
		if (list == null) {
			list = new ArrayList<Sprite>();
			sprites.put(zIndex, list);
		}
		list.add(s);
	}
	
	public Map<Integer, List<Sprite>> getSprites() {	
		return sprites;
	}
	
	public int getSpriteCount() {
		int size = 0;
		for (List<Sprite> list: sprites.values()) {
			size += list.size();
		}
		return size;
	}

	@Override
	public void resize(int width, int height) {
		this.setViewport(width / scale, height / scale, false);
		
		for (ResizeListener listener: resizeListeners) {
			listener.resize( (int)(width/scale), (int)(height/scale) );
		}
	}

	/**
	 * Retrieve a list of all entities that have the given 'type'.
	 * @param tag
	 * @return
	 */
	public List<Entity> getEntitiesByTag(String tag) {
		List<Entity> list = this.entityTags.get(tag);
		
		// If list doesn't exist, return an empty List so iteration still works.
		if (list == null) {
			list = new ArrayList<Entity>();
		}
		
		return list;
	}
	
	/**
	 * Retrieve a single entity of the given type.
	 * Only use this if you know there is only the one entity.
	 * @param tag
	 * @return The entity, or null if no entities have the tag.
	 */
	public Entity getEntityByTag(String tag) {
		List<Entity> list = this.entityTags.get(tag);
		
		if (list == null) {
			return null;
		} else if (list.isEmpty()) {
			return null;
		}
		
		return list.get(0);
	}
	
	/**
	 * Return the Entity of the given type that is closest to 'entity'
	 * @param tag
	 * @param entity
	 * @return
	 */
	public Entity getClosestEntity(String tag, Entity entity) {
		
		// Entity must have a position, or this makes no sense
		if (entity.getComponent(PositionComponent.class) == null) {
			return null;
		}
		
		// If no entities have the given type, return null
		List<Entity> list = getEntitiesByTag(tag);
		if (list == null) {
			return null;
		}
		
		Entity nearest = null;
		Vector2 position = entity.getComponent(PositionComponent.class).getPosition();
		Vector2 otherPosition;
		float distance = -1, tmp;
		for (Entity e: list) {
			// If no position, skip it
			if (e.getComponent(PositionComponent.class) == null)
				continue;
			
			// Don't return the requesting entity
			if (e == entity)
				continue;
			
			// Nothing to compare to yet, use this for now.
			if (distance < 0) {
				nearest = e;
				otherPosition = e.getComponent(PositionComponent.class).getPosition();
				distance = position.dst(otherPosition);
			} else {
				otherPosition = e.getComponent(PositionComponent.class).getPosition();
				tmp = position.dst(otherPosition);
				if (tmp < distance) {
					nearest = e;
					distance = tmp;
				}
			}
		}
		
		return nearest;
	}
	
	/**
	 * Return the number of actors contained in this scene.
	 * @return
	 */
	public int getActorCount() {
		return getActors().size;
	}
	
	/**
	 * Add a listener object, which will be notified when the scene is resized.
	 * @param listener
	 */
	public void addResizeListener(ResizeListener listener) {
		this.resizeListeners.add(listener);
	}
	
	public float getCameraLeft() { return getCamera().position.x - getCamera().viewportWidth/2; }
	public float getCameraRight() { return getCamera().position.x + getCamera().viewportWidth/2; }
	public float getCameraTop() { return getCamera().position.y + getCamera().viewportHeight/2; }
	public float getCameraBottom() { return getCamera().position.y - getCamera().viewportHeight/2; }
	
	/**
	 * Get the mouse's coordinates in stage space.
	 * @return
	 */
	public Vector2 getMouseStagePosition() {
		return getMouseScreenPosition().add(getCameraLeft(), getCameraBottom());
	}
	
	/**
	 * Get the mouse's position in screen space
	 * @return
	 */
	public Vector2 getMouseScreenPosition() {
		return new Vector2(
				Gdx.input.getX() / scale,
				(Gdx.graphics.getHeight() - Gdx.input.getY()) / scale
		);
	}
	
	public IEntityFactory getEntityFactory() {
		return null;
	}

	/**
	 * Set the bounding rectangle for the camera. It will be prevented from seeing outside this area.
	 * @param left
	 * @param right
	 * @param top
	 * @param bottom
	 */
	public void constrainCamera(int left, int right, int top, int bottom) {
		this.cameraBounds.set(left, bottom, right - left, top - bottom);
		this.cameraBounded = true;
	}

	/**
	 * Keep the camera in the bounding box, and refresh it.
	 */
	public void setCameraPosition(float x, float y) {
		Camera camera = getCamera();
		camera.position.x = x;
		camera.position.y = y;
		
		keepCameraInBounds();
	}
	
	/**
	 * Shift the camera position by a relative amount
	 * @param xDiff
	 * @param yDiff
	 */
	public void moveCamera(float xDiff, float yDiff) {
		Camera camera = getCamera();
		camera.position.x += xDiff;
		camera.position.y += yDiff;
		
		keepCameraInBounds();
	}
	
	private void keepCameraInBounds() {
		if (cameraBounded) {
			Camera camera = getCamera();
			
			if (getCameraLeft() < cameraBounds.x) {
				camera.position.x = cameraBounds.x + camera.viewportWidth/2;
			} else if (getCameraRight() > cameraBounds.x + cameraBounds.width) {
				camera.position.x = cameraBounds.x + cameraBounds.width - camera.viewportWidth/2;
			}
			
			if (getCameraBottom() < cameraBounds.y) {
				camera.position.y = cameraBounds.y + camera.viewportHeight/2;
			} else if (getCameraTop() > cameraBounds.y + cameraBounds.height) {
				camera.position.y = cameraBounds.y + cameraBounds.height - camera.viewportHeight/2;
			}
		}
	}

	/**
	 * Set the scale factor.
	 * e.g., a scale of 2 would have everything drawn at twice the width and height
	 * @param scale
	 */
	public void setScale(float scale) {
		float oldScale = this.scale;
		this.scale = scale;
		
		// Resize accordingly
		resize( (int)(getWidth() * oldScale), (int)(getHeight() * oldScale) );
	}
	
	/**
	 * Returns a new EntityFilter containing all the entities in the scene.
	 * @return
	 */
	public EntityFilter getEntityFilter() {
		return new EntityFilter(entities);
	}

	/**
	 * Get a list of all entities that overlap the given position
	 * @param position
	 * @return
	 */
	public EntityFilter getEntitiesAtPosition(Vector2 position) {
		return new EntityFilter(entities)
						.overlappingPosition(position);
	}

	/**
	 * Get a list of all entities that overlap the given position and have the given tag
	 * @param position
	 * @param tag
	 * @return
	 */
	public EntityFilter getEntitiesAtPosition(Vector2 position, String tag) {
		return new EntityFilter(getEntitiesByTag(tag))
						.overlappingPosition(position);
	}
	
	public G getGame() {
		return (G)this.game;
	}
	
	/**
	 * Turn debug mode on or off.
	 * @param enabled
	 */
	public void setDebugMode(boolean enabled) {
		debugMode = enabled;
		if (enabled && debugShapeRenderer == null) {
			debugShapeRenderer = new ShapeRenderer();
			fpsLogger = new FPSLogger();
		}
	}
	
	public boolean isDebugModeEnabled() {
		return debugMode;
	}
}
