package uk.co.samatkins;

public interface IEntityFactory {
	public Entity create(String name, Object ...parameters);
}
