package uk.co.samatkins;

import java.util.Collection;
import java.util.Iterator;

/**
 * Implementation of a sorted linked list.
 * 
 * Elements must implement Comparable.
 * 
 * @author Sam
 *
 * @param <T>
 */
public class SortedList<T extends Comparable<T>> implements Collection<T> {
	
	private Element<T> first;
	private int size;

	public SortedList() {
		first = null;
		size = 0;
	}
	
	@Override
	public Iterator<T> iterator() {
		return new SortedListIterator(this);
	}

	@Override
	public boolean add(T element) {
		size++;
		if (first == null) {
			first = new Element<T>(element);
		} else {
			Element<T> newElement = new Element<T>(element);
			Element<T> current = first,
					previous = null;
			
			// While the new element is greater than the current
			while (newElement.compareTo(current) > 0) {
				if (current.next == null) {
					// We reached the end of the list, so add new on the end.
					current.next = newElement;
					return true;
				} else {
					// Continue iterating
					previous = current;
					current = current.next;
				}
			}
			// If we got here, then new goes between current and previous
			if (previous == null) {
				// Smaller than the first element, so put it first
				first = newElement;
				newElement.next = current;
			} else {
				previous.next = newElement;
				newElement.next = current;
			}
		}
		
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends T> collection) {
		
		for (T item: collection) {
			this.add(item);
		}
		
		return true;
	}
	
	public T first() {
		if (this.first == null) {
			return null;
		}
		return this.first.value;
	}

	@Override
	public void clear() {
		first = null;
		size = 0;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public boolean contains(Object value) {
		try {
			T test = (T) value;
		} catch (ClassCastException e) {
			return false;
		}
		
		for (T item: this) {
			if (item.equals(value)) {
				return true;
			} else if ( item.compareTo((T)value) > 0) {
				return false;
			}
		}
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> collection) {
		for (Object o: collection) {
			if (!contains(o)) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public boolean remove(Object value) {
		T current;
		for (Iterator<T> it = iterator(); it.hasNext(); ) {
			current = it.next();
			if (current.equals(value)) {
				it.remove();
				return true;
			}
		}
		
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> collection) {
		int oldSize = size();
		boolean b;
		for (Object o: collection) {
			do {
				b = remove(o);
			} while (b);
		}
		
		return size() != oldSize;
	}

	@Override
	public boolean retainAll(Collection<?> collection) {
		int oldSize = size();
		boolean b;
		
		for (T item: this) {
			if (!collection.contains(item)) {
				do {
					b = remove(item);
				} while (b);
			}
		}
		
		return size() != oldSize;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public Object[] toArray() {
		Object[] array = new Object[this.size()];
		
		int i=0;
		for (T item: this) {
			array[i++] = item;
		}
		return array;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T2> T2[] toArray(T2[] array) {
		if (array.length <= size()) {
			int i=0;
			for (T item: this) {
				array[i++] = (T2) item;
			}
			return array;
		} else {
			return (T2[]) this.toArray();
		}
	}
	
	private class SortedListIterator implements Iterator<T> {
		private SortedList<T> list;
		private Element<T> previous, current;
		
		public SortedListIterator(SortedList<T> list) {
			this.list = list;
		}

		@Override
		public boolean hasNext() {
			if (current == null) {
				return list.first != null;
			}
			return current.next != null;
		}

		@Override
		public T next() {
			previous = current;
			if (current == null) {
				current = list.first;
			} else {
				current = current.next;
			}
			return current == null ? null : current.value;
		}

		@Override
		public void remove() {
			if (previous == null) {
				list.first = current.next;
			} else {
				previous.next = current.next;
			}
			list.size--;
		}
		
	}

	@SuppressWarnings("hiding")
	private class Element<T extends Comparable<T>> implements Comparable<Element<T>> {
		
		private Element<T> next;
		private T value;
		
		public Element(T value) {
			this.value = value;
		}
		
		@Override
		public int compareTo(Element<T> other) {
			return this.value.compareTo(other.value);
		}
		
	}
}
