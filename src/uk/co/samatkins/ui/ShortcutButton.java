package uk.co.samatkins.ui;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Button that can be triggered both by clicking and with a key press.
 * Currently very basic, allowing only simple typeable keys.
 * The button's parent MUST be the keyboard focus for this to work.
 * 
 * @author Sam
 *
 */
public class ShortcutButton extends Button {
	public interface IClickCallback {
		public void clicked();
	};
	
	private char shortcut;
	private IClickCallback callback;
	
	public ShortcutButton(String label, char shortcut, Skin skin, IClickCallback onClick) {
		super(skin);
		this.shortcut = shortcut;
		this.callback = onClick;
		
		this.add(label + " (" + shortcut + ")");
		
		addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				callback.clicked();
			}
		});
	}
	
	@Override
	protected void setParent(Group parent) {
		super.setParent(parent);
		parent.addListener(new InputListener() {
			@Override
			public boolean keyTyped(InputEvent event, char character) {
				if (character == shortcut) {
					callback.clicked();
					return true;
				}
				
				return false;
			}
		});
	}
}
