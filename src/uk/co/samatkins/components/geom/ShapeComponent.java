package uk.co.samatkins.components.geom;

import uk.co.samatkins.Entity;
import uk.co.samatkins.components.AbstractComponent;
import uk.co.samatkins.geom.Shape;

/**
 * Component for associating a Shape with an Entity.
 * This is now deprecated - instead, just assign a Shape to
 * the PositionComponent.
 * @author Sam
 *
 */
@Deprecated
public class ShapeComponent extends AbstractComponent {
	
	private PositionComponent position;
	
	private float offsetX, offsetY;
	private Shape shape;

	public ShapeComponent(Shape shape) {
		this.shape = shape;
		offsetX = shape.getLeft();
		offsetY = shape.getBottom();
	}
	
	@Override
	public void added(Entity entity) {
		super.added(entity);
		
		position = require(PositionComponent.class);
		recalculatePosition();
	}
	
	public Shape getShape() {
		return this.shape;
	}
	
	@Override
	public void lateUpdate(float delta) {
		super.lateUpdate(delta);
		
		recalculatePosition();
	}

	public void recalculatePosition() {
		shape.setLeft(position.getX() + offsetX);
		shape.setBottom(position.getY() + offsetY);
	}

}
