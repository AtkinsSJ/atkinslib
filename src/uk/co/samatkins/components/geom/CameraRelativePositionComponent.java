package uk.co.samatkins.components.geom;

import uk.co.samatkins.Entity;
import uk.co.samatkins.components.AbstractComponent;

/**
 * Component that keeps its entity at a position relative to the camera.
 * @author Sam
 *
 */
public class CameraRelativePositionComponent extends AbstractComponent {
	
	private PositionComponent position;
	
	private boolean stickToTop = false,
					stickToBottom = false,
					stickToLeft = false,
					stickToRight = false;
	private float topDistance,
				  bottomDistance,
				  leftDistance,
				  rightDistance;

	public CameraRelativePositionComponent() {
		
	}
	
	/**
	 * Set the distance from the left edge of the camera
	 * @param distance
	 */
	public void setLeftDistance(float distance) {
		stickToLeft = true;
		leftDistance = distance;
	}
	
	/**
	 * Set the distance from the right edge of the camera
	 * @param distance
	 */
	public void setRightDistance(float distance) {
		stickToRight = true;
		rightDistance = distance;
	}
	
	/**
	 * Set the distance from the top edge of the camera
	 * @param distance
	 */
	public void setTopDistance(float distance) {
		stickToTop = true;
		topDistance = distance;
	}
	
	/**
	 * Set the distance from the bottom edge of the camera
	 * @param distance
	 */
	public void setBottomDistance(float distance) {
		stickToBottom = true;
		bottomDistance = distance;
	}
	
	@Override
	public void added(Entity entity) {
		super.added(entity);
		
		position = require(PositionComponent.class);
	}
	
	@Override
	public void lateUpdate(float delta) {
		super.lateUpdate(delta);
		
		float x = position.getX(),
			  y = position.getY();
		
		if (stickToLeft) {
			x = getScene().getCameraLeft() + leftDistance;
		} else if (stickToRight) {
			x = getScene().getCameraRight() - rightDistance;
		}
		
		if (stickToTop) {
			y = getScene().getCameraTop() - topDistance;
		} else if (stickToBottom) {
			y = getScene().getCameraBottom() + bottomDistance;
		}
		
		position.setPosition(x, y);
	}
}
