package uk.co.samatkins.components.geom;

import uk.co.samatkins.components.AbstractComponent;
import uk.co.samatkins.geom.Point;
import uk.co.samatkins.geom.Shape;

import com.badlogic.gdx.math.Vector2;

/**
 * Component dealing with the physical presence of an Entity.
 * By default, this is just an x,y coordinate.
 * Use {@link #setShape(Shape)) to use a different Shape.
 * @author Sam
 *
 */
public class PositionComponent extends AbstractComponent {
	
	private Shape shape;
	
	public PositionComponent() {
		this(Vector2.Zero);
	}

	public PositionComponent(Vector2 position) {
		this.shape = new Point(position.x, position.y);
	}
	
	public PositionComponent(Shape shape) {
		this.shape = shape;
	}
	
	public Vector2 getPosition() {
		return shape.getCentre();
	}
	
	public float getX() {
		return shape.getCentreX();
	}
	
	public float getY() {
		return shape.getCentreY();
	}

	public void setPosition(Vector2 position) {
		shape.setCentre(position.x, position.y);
	}
	
	public void setPosition(float x, float y) {
		shape.setCentre(x,y);
	}
	
	/**
	 * Move by the given amount
	 * @param movement
	 */
	public void moveBy(Vector2 movement) {
		shape.setLeft(shape.getLeft() + movement.x);
		shape.setBottom(shape.getBottom() + movement.y);
	}

	/**
	 * Move by the given amount
	 * @param x
	 * @param y
	 */
	public void moveBy(float x, float y) {
		shape.setLeft(shape.getLeft() + x);
		shape.setBottom(shape.getBottom() + y);
	}
	
	/**
	 * Replaces the current Shape with the given one.
	 * @param shape
	 */
	public void setShape(Shape shape) {
		this.shape = shape;
	}
	
	/**
	 * @return The Shape
	 */
	public Shape getShape() {
		return shape;
	}

}
