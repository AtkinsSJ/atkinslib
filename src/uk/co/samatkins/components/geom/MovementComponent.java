package uk.co.samatkins.components.geom;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.OrderedMap;

import uk.co.samatkins.Entity;
import uk.co.samatkins.components.AbstractComponent;

public class MovementComponent extends AbstractComponent {
	
	public interface IMovementCompleteListener {
		public void complete();
	}
	
	PositionComponent positionComponent;
	
	private float speed;
	
	private Vector2 target;
	private boolean moveToTarget;
	private IMovementCompleteListener callback;
	
	// Used to temporarily store movement vector, prevent recreating vectors
	private Vector2 movement = new Vector2(); 

	public MovementComponent() {
		this(0);
	}
	
	public MovementComponent(float speed) {
		this.speed = speed;
		target = new Vector2();
		moveToTarget = false;
	}
	
	@Override
	public void added(Entity entity) {
		super.added(entity);
		positionComponent = require(PositionComponent.class);
	}

	@Override
	public void update(float delta) {
		float movementDistance = speed * delta;
		
		// Are we moving toward a target?
		if (moveToTarget) {
			// Are we near the target?
			if (positionComponent.getPosition().dst(target) < movementDistance) {
				positionComponent.setPosition(target);
				moveToTarget = false;
				if (callback != null) {
					callback.complete();
				}
			} else {
				// Move 'movementDistance' towards the target
				movement.set(movementDistance, 0);
				// Set angle
				float angle = MathUtils.atan2(
						target.y - positionComponent.getY(),
						target.x - positionComponent.getX());
				movement.setAngle(angle * MathUtils.radiansToDegrees);
				positionComponent.moveBy(movement);
			}
		}
	}
	
	/**
	 * Set the maximum speed, in units per second.
	 * @param speed
	 */
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	
	public float getSpeed() {
		return this.speed;
	}
	
	/**
	 * Start to move towards the given position
	 * @param targetPosition
	 */
	public void setTargetPosition(Vector2 targetPosition) {
		setTargetPosition(targetPosition, null);
	}

	/**
	 * Start to move towards the given position
	 * @param targetPosition
	 * @param callback
	 */
	public void setTargetPosition(Vector2 targetPosition, IMovementCompleteListener callback) {
		this.target.set(targetPosition);
		this.moveToTarget = true;
		this.callback = callback;
	}
	
	/**
	 * Start to move towards the given relative position
	 * @param relativePosition
	 */
	public void setRelativeTarget(Vector2 relativePosition) {
		setTargetPosition(
				this.positionComponent.getPosition().cpy().add(relativePosition)
		);
	}
	
	/**
	 * Is this component currently moving towards a target?
	 * @return
	 */
	public boolean hasTarget() {
		return moveToTarget;
	}

	/**
	 * Stop moving. Clears the movement target.
	 */
	public void cancelMovement() {
		moveToTarget = false;
	}

	@Override
	public void write(Json json) {
		super.write(json);
		// TODO: Implement writing to Json
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		super.read(json, jsonData);
		
		// TODO: Implement loading from Json
	}
}
