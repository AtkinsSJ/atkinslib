package uk.co.samatkins.components.geom;

import java.util.List;

import uk.co.samatkins.Entity;
import uk.co.samatkins.Scene;
import uk.co.samatkins.components.AbstractComponent;
import uk.co.samatkins.geom.Shape;
import uk.co.samatkins.geom.ShapeCollider;

/**
 * Causes the Entity to collide with the given ColliderGridComponent.
 * @author Sam
 *
 */
public class ColliderComponent extends AbstractComponent {
	
	private PositionComponent position;
	
	private String gridEntityTag;
	private ColliderGridComponent grid;

	public ColliderComponent(String gridEntityTag) {
		this.gridEntityTag = gridEntityTag;
	}
	
	@Override
	public void added(Entity entity) {
		super.added(entity);
		
		position = require(PositionComponent.class);
	}
	
	@Override
	public void addedToScene(Scene<?> scene) {
		super.addedToScene(scene);
		
		Entity e = scene.getEntityByTag(gridEntityTag);
		if (e == null) {
			throw new RuntimeException("No Entity with the tag \""+gridEntityTag+"\" found.");
		}
		
		grid = e.getComponent(ColliderGridComponent.class);
	}
	
	@Override
	public void lateUpdate(float delta) {
		super.lateUpdate(delta);
		
		Shape shape = position.getShape();
		List<Shape> colliders = grid.getCollidersAt(
				shape.getLeft(), shape.getBottom(),
				shape.getWidth(), shape.getHeight());
		
		// For each collider
		for (Shape collider: colliders) {
			// Do we intersect with it?
			if (ShapeCollider.doesIntersect(shape, collider)) {
				// If so, move accordingly.
				position.moveBy(ShapeCollider.calculateExitVector(shape, collider));
			}
		}
	}

}
