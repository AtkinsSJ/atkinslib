package uk.co.samatkins.components.geom;

import com.badlogic.gdx.math.MathUtils;

import uk.co.samatkins.Entity;
import uk.co.samatkins.Scene;
import uk.co.samatkins.Scene.ResizeListener;
import uk.co.samatkins.components.AbstractComponent;

/**
 * Clamps the position to be within the bounds of the scene.
 * @author Sam
 *
 */
public class StayOnScreenComponent extends AbstractComponent
									implements ResizeListener {
	
	private PositionComponent position;
	private float padding;
	private float left, right, bottom, top;

	public StayOnScreenComponent(float padding) {
		this.padding = padding;
	}
	
	@Override
	public void added(Entity entity) {
		super.added(entity);
		
		position = require(PositionComponent.class);
	}
	
	@Override
	public void addedToScene(Scene<?> scene) {
		super.addedToScene(scene);
		
		scene.addResizeListener(this);
	}

	@Override
	public void resize(int width, int height) {
		left = padding;
		right = width - padding;
		bottom = padding;
		top = height - padding;
	}
	
	@Override
	public void lateUpdate(float delta) {
		super.lateUpdate(delta);
		
		position.setPosition( MathUtils.clamp(position.getX(), left, right),
								MathUtils.clamp(position.getY(), bottom, top));
	}
}
