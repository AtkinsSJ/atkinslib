package uk.co.samatkins.components.geom;

import java.util.ArrayList;
import java.util.List;

import uk.co.samatkins.Entity;
import uk.co.samatkins.components.AbstractComponent;
import uk.co.samatkins.geom.Circle;
import uk.co.samatkins.geom.Line;
import uk.co.samatkins.geom.RATriangle;
import uk.co.samatkins.geom.Rectangle;
import uk.co.samatkins.geom.Shape;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

/**
 * Maintains a 2d array of collision polygons.
 * 
 * @author Sam
 *
 */
public class ColliderGridComponent extends AbstractComponent {
	
	private Vector2 oldPosition;
	private PositionComponent position;
	
	private int tilesH, tilesV,
				tileWidth, tileHeight;

	private Shape[][] shapes;

	public ColliderGridComponent(int tilesH, int tilesV,
			int tileWidth, int tileHeight) {
		
		this.tilesH = tilesH;
		this.tilesV = tilesV;
		this.tileHeight = tileHeight;
		this.tileWidth = tileWidth;
		
		this.shapes = new Shape[tilesH][tilesV];
	}
	
	@Override
	public void added(Entity entity) {
		super.added(entity);
		
		position = require(PositionComponent.class);
		oldPosition = position.getPosition().cpy();
	}
	
	@Override
	public void lateUpdate(float delta) {
		super.lateUpdate(delta);
		
		Vector2 newPosition = position.getPosition();
		if (!newPosition.epsilonEquals(oldPosition, 0.01f)) {
			oldPosition.set(newPosition);
			
			// Set all the shape positions
			Shape shape;
			for (int x=0; x<tilesH; x++) {
				for (int y=0; y<tilesV; y++) {
					shape = getColliderAt(x, y);
					if (shape != null) {
						shape.setLeft((x*tileWidth) + position.getX());
						shape.setBottom((y*tileHeight) + position.getY());
					}
				}
			}
		}
	}
	
	/**
	 * Returns whether the given tile coordinates exist in the grid.
	 * @param x
	 * @param y
	 * @return
	 */
	private boolean tileValid(int x, int y) {
		return (x >= 0 && y >= 0 && x < tilesH && y < tilesV);
	}
	
	/**
	 * Fill the grid cell at x,y with a rectangle.
	 * @param x
	 * @param y
	 * @return The created Rectangle, or null if the position is invalid
	 */
	public Rectangle putRectangle(int x, int y) {
		if (!tileValid(x,y)) {
			return null;
		}
		Rectangle r = new Rectangle(
				(x*tileWidth) + position.getX(),
				(y*tileHeight) + position.getY(),
				tileWidth, tileHeight);
		shapes[x][y] = r;
		
		return r;
	}
	
	/**
	 * Fill the grid cell at x,y with a Circle
	 * @param x
	 * @param y
	 * @return The newly-created Circle
	 */
	public Circle putCircle(int x, int y) {
		if (!tileValid(x,y)) {
			return null;
		}
		Circle c = new Circle(
				(((float)x+0.5f) * tileWidth) + position.getX(),
				(((float)y+0.5f) * tileHeight) + position.getY(),
				tileWidth/2f );
		shapes[x][y] = c;
		
		return c;
	}
	
	/**
	 * Fill the grid cell at x,y with a RATriangle
	 * @param x
	 * @param y
	 * @param leftSolid
	 * @param topSolid
	 * @return The newly-created RATriangle
	 */
	public RATriangle putRATriangle(int x, int y, boolean leftSolid, boolean topSolid) {
		if (!tileValid(x,y)) {
			return null;
		}
		RATriangle t = new RATriangle(
				(x*tileWidth) + position.getX(),
				(y*tileHeight) + position.getY(),
				tileWidth, tileHeight, 
				leftSolid, topSolid);
		shapes[x][y] = t;
		
		return t;
	}
	
	/**
	 * Get the coordinates for the tile at the given stage position.
	 * Caution: No checks are done to ensure the tile exists!
	 * @param stageCoordinates
	 * @return
	 */
	public Vector2 getTileCoordinatesAt(Vector2 stageCoordinates) {
		// To get grid position, subtract tilemap position from desired stage position, then divide by the tile size
		return stageCoordinates
					.sub(position.getPosition())
					.sub(stageCoordinates.x % tileWidth, stageCoordinates.y % tileHeight)
					.div(tileWidth, tileHeight);
	}
	
	/**
	 * Obtain the collider at grid position x,y, or null if the cell is empty.
	 * @param x
	 * @param y
	 * @return
	 */
	public Shape getColliderAt(int x, int y) {
		if (!tileValid(x, y)) return null;
		return shapes[x][y];
	}
	
	/**
	 * Obtain all colliders from cells overlapped by the given area.
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @return A list, that may be empty.
	 */
	public List<Shape> getCollidersAt(float x, float y, float width, float height) {
		Vector2 start = getTileCoordinatesAt( new Vector2(x,y) ),
				end = getTileCoordinatesAt( new Vector2(x+width, y+height) );
		
		List<Shape> list = new ArrayList<Shape>();
		
		Shape shape;
		for (int yi = (int) start.y; yi<=end.y; yi++) {
			for (int xi = (int) start.x; xi<=end.x; xi++) {
				shape = getColliderAt(xi, yi);
				if (shape != null) list.add(shape);
			}
		}
		
		return list;
	}
	
	/**
	 * Draw outlines of all colliders
	 * @param shapeRenderer
	 */
	public void debugRender(ShapeRenderer shapeRenderer) {
		
		shapeRenderer.setColor(1, 0, 0, 1);
		
		Shape shape;
		
		shapeRenderer.begin(ShapeType.Line);
		for (int x=0; x<tilesH; x++) {
			for (int y=0; y<tilesV; y++) {
				shape = getColliderAt(x, y);
				if (shape != null) {
					if (shape.getType().equals(Shape.ShapeType.RECTANGLE)) {
						shapeRenderer.rect(shape.getLeft(), shape.getBottom(),
								shape.getWidth(), shape.getHeight());
					}
				}
			}
		}
		shapeRenderer.end();
		
		Circle circle;
		shapeRenderer.begin(ShapeType.Line);
		for (int x=0; x<tilesH; x++) {
			for (int y=0; y<tilesV; y++) {
				shape = getColliderAt(x, y);
				if (shape != null) {
					if (shape.getType().equals(Shape.ShapeType.CIRCLE)) {
						circle = (Circle) shape;
						shapeRenderer.circle(circle.x, circle.y, circle.radius);
					}
				}
			}
		}
		shapeRenderer.end();
		
		RATriangle triangle;
		Line line;
		shapeRenderer.begin(ShapeType.Line);
		for (int x=0; x<tilesH; x++) {
			for (int y=0; y<tilesV; y++) {
				shape = getColliderAt(x, y);
				if (shape != null) {
					if (shape.getType().equals(Shape.ShapeType.RATRIANGLE)) {
						triangle = (RATriangle) shape;
						line = triangle.getAngleLine();
						shapeRenderer.line(line.start.x, line.start.y, line.end.x, line.end.y);
					}
				}
			}
		}
		shapeRenderer.end();
	}

	/**
	 * Clear the grid of colliders
	 */
	public void clearColliders() {
		shapes = new Shape[tilesH][tilesV];
	}

}
