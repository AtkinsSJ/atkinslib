package uk.co.samatkins.components;

public class Node implements Comparable<Node> {
	public int x, y;
	public int h;
	public Node parent;
	public Node(){}
	
	public Node(int x, int y, int goalX, int goalY, Node parent) {
		this.x = x;
		this.y = y;
		this.h = Math.abs(x-goalX) + Math.abs(y-goalY);
		this.parent = parent;
	}
	
	public int g() {
		if (parent == null) return 0;
		return parent.g() + 1;
	}
	
	public void set(Node parent) {
		this.parent = parent;
	}
	
	@Override
	public int compareTo(Node other) {
		return (g()+h) - (other.g()+other.h);
	}
}
