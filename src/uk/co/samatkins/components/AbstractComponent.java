package uk.co.samatkins.components;

import com.badlogic.gdx.utils.JsonValue;
import uk.co.samatkins.Entity;
import uk.co.samatkins.Scene;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.OrderedMap;

public abstract class AbstractComponent
				implements IComponent,
							Json.Serializable {
	
	private Entity entity;
	private Scene<?> scene;

	@Override
	public void added(Entity entity) {
		this.entity = entity;
	}
	
	public Entity getEntity() {
		return entity;
	}
	
	public Scene<?> getScene() {
		return scene;
	}
	
	public void addedToScene(Scene<?> scene) {
		this.scene = scene;
	}
	
	public void removedFromScene(Scene<?> scene) {
		this.scene = null;
	}
	
	@Override
	public void update(float delta) {
		
	}
	
	@Override
	public void lateUpdate(float delta) {
		
	}
	
	@Override
	public void render(float delta) {
		
	}
	
	/**
	 * Ensure that the given component exists in the entity, by
	 * checking for it and creating it if necessary.
	 * 
	 * If the component lacks a no-argument constructor, it cannot
	 * be added, and this will return null.
	 * 
	 * @param c
	 * @return The component, or null
	 */
	@SuppressWarnings("unchecked")
	public <T extends IComponent> T require(Class<T> c) {
		IComponent component = entity.getComponent(c);
		if (component == null) {
			// Add the component
			try {
				entity.addComponent(c.newInstance());
				component = entity.getComponent(c);
			} catch (InstantiationException e) {
				
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				
				e.printStackTrace();
			}
		}
		return (T) component;
	}
	
	@Override
	public void write(Json json) { }

	@Override
	public void read(Json json, JsonValue jsonData) { }
}
