package uk.co.samatkins.components;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.OrderedMap;

public class PositionComponent extends AbstractComponent {
	
	private Vector2 position;
	
	public PositionComponent() {
		this(Vector2.Zero);
	}

	public PositionComponent(Vector2 position) {
		this.position = position.cpy();
	}
	
	public Vector2 getPosition() {
		return new Vector2(position);
	}

	public void setPosition(Vector2 position) {
		this.position.set(position);
	}
	
	public void setPosition(float x, float y) {
		this.position.set(x, y);
	}
	
	/**
	 * Move by the given amount
	 * @param movement
	 */
	public void moveBy(Vector2 movement) {
		this.position.add(movement);
	}

	@Override
	public void update(float delta) {
		
	}

	@Override
	public void write(Json json) {
		super.write(json);
		json.writeValue("position", position);
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		super.read(json, jsonData);
        JsonValue pos = jsonData.get("position");
		position = new Vector2(pos.getFloat("x"), pos.getFloat("y"));
	}
}
