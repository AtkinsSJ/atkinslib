package uk.co.samatkins.components.ui;

import uk.co.samatkins.Scene;
import uk.co.samatkins.components.AbstractComponent;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class MainMenuComponent extends AbstractComponent {

	private Table table;
	
	public MainMenuComponent() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void addedToScene(Scene<?> scene) {
		super.addedToScene(scene);
		
		table = new Table(scene.getGame().getSkin());
		table.setFillParent(true);
		
		scene.addActor(table);
		
		setupMenu(scene.getGame().getSkin());
	}
	
	@Override
	public void removedFromScene(Scene<?> scene) {
		super.removedFromScene(scene);
		
		table.remove();
	}

	/**
	 * Helper method to create a TextButton and add it to the Menu.
	 * @param text
	 * @param listener
	 */
	public void addButton(String text, ClickListener listener) {
		TextButton button = new TextButton(
				text, getScene().getGame().getSkin(), "menuButton");
		button.addListener(listener);
		
		table.add(button).row();
	}
	
	/**
	 * Called after creating the layout table.
	 * Override this in child classes and define the menu.
	 * @param skin
	 */
	protected void setupMenu(Skin skin) {
		
	}

}
