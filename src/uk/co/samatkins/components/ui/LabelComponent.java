package uk.co.samatkins.components.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

import uk.co.samatkins.Entity;
import uk.co.samatkins.Scene;
import uk.co.samatkins.components.AbstractComponent;
import uk.co.samatkins.components.geom.PositionComponent;

public class LabelComponent extends AbstractComponent {
	
	private String text;
	private int alignment;
	
	private Label label;
	private Color color = Color.WHITE.cpy();
	
	private PositionComponent position;

	public LabelComponent(String text) {
		this(text, Align.center);
	}
	
	public LabelComponent(String text, int alignment) {
		this.text = text;
		this.alignment = alignment;
	}
	
	@Override
	public void added(Entity entity) {
		super.added(entity);
		
		position = require(PositionComponent.class);
	}
	
	@Override
	public void addedToScene(Scene<?> scene) {
		super.addedToScene(scene);
		
		label = new Label(text, scene.getGame().getSkin());
		label.setZIndex(1000);
		label.setColor(color);
		setSizeAndOrigin();

		label.setPosition(position.getX() - label.getOriginX(),
						position.getY() - label.getOriginY() );
		
		scene.addActor(label);
	}
	
	@Override
	public void removedFromScene(Scene<?> scene) {
		super.removedFromScene(scene);
		
		label.remove();
		
	}

	public void setText(String newText) {
		text = newText;
		
		if (label != null) {
			label.setText(newText);
			setSizeAndOrigin();
		}
	}
	
	public void setColor(float r, float g, float b, float a) {
		color.set(r, g, b, a);
		if (label != null) {
			label.setColor(color);
		}
	}
	
	private void setSizeAndOrigin() {
		TextBounds bounds = label.getTextBounds();
		label.setSize(bounds.width, bounds.height);
		label.setAlignment(alignment);
		
		switch (alignment) {
		case Align.center:
			label.setOrigin(bounds.width/2, bounds.height/2);
			break;
		case Align.left:
			label.setOrigin(0, bounds.height/2);
			break;
		case Align.right:
			label.setOrigin(bounds.width, bounds.height/2);
			break;
		case Align.top:
			label.setOrigin(bounds.width/2, bounds.height);
			break;
		case Align.bottom:
			label.setOrigin(bounds.width/2, 0);
			break;
		}
	}
	
	@Override
	public void lateUpdate(float delta) {
		super.lateUpdate(delta);
		
		label.setPosition(position.getX() - label.getOriginX(),
						position.getY() - label.getOriginY() );
	}
}
