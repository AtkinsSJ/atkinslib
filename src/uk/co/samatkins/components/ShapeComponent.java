package uk.co.samatkins.components;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.OrderedMap;

import uk.co.samatkins.geom.Shape;

public class ShapeComponent extends AbstractComponent {
	
	private Shape shape;

	public ShapeComponent(Shape shape) {
		this.shape = shape;
	}
	
	public Shape getShape() {
		return this.shape;
	}

	@Override
	public void write(Json json) {
		super.write(json);
		json.writeValue("shapeClass", this.shape.getClass().getCanonicalName());
		json.writeValue("shape", this.shape);
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		super.read(json, jsonData);
		String shapeClass = jsonData.getString("shapeClass");
		try {
			this.shape = (Shape) Class.forName(shapeClass).cast( jsonData.get("shape") );
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
