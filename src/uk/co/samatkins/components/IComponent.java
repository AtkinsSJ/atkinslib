package uk.co.samatkins.components;

import uk.co.samatkins.Entity;
import uk.co.samatkins.Scene;

public interface IComponent {
	
	/**
	 * Called when added to the given entity.
	 * @param entity
	 */
	public void added(Entity entity);
	
	public Entity getEntity();
	
	/**
	 * Called when the entity is added to the given scene.
	 * @param scene
	 */
	public void addedToScene(Scene<?> scene);
	
	/**
	 * Called once per frame. Run any code for this component.
	 * @param delta
	 */
	public void update(float delta);
	
	/**
	 * Called once per frame, after update() has been called on all of
	 * this entity's components.
	 * @param delta
	 */
	public void lateUpdate(float delta);
	
	/**
	 * Final update methods, called after update and lateUpdate.
	 * Should only be used for drawing things.
	 * @param delta
	 */
	public void render(float delta);
	
	/**
	 * Called just before the entity is removed from the scene.
	 * Clean-up anything that isn't contained within the component.
	 * @param scene
	 */
	public void removedFromScene(Scene<?> scene);
}
