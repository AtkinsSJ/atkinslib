package uk.co.samatkins.components.image;

import uk.co.samatkins.Scene;
import uk.co.samatkins.Scene.ResizeListener;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * A {@link TiledSpriteComponent} that automatically resizes to cover
 * the screen.
 * 
 * @author Sam
 *
 */
public class BackgroundTiledSpriteComponent extends TiledSpriteComponent
											implements ResizeListener {

	public BackgroundTiledSpriteComponent(TextureRegion textureRegion, int zIndex) {
		super(textureRegion, 1, 1);
		setZIndex(zIndex);
	}
	
	@Override
	public void addedToScene(Scene<?> scene) {
		super.addedToScene(scene);
		scene.addResizeListener(this);
	}

	@Override
	public void resize(int width, int height) {
		tilesH = (width / tileW) + 1;
		tilesV = (height / tileH) + 1;
	}

}
