package uk.co.samatkins.components.image;


import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * A {@link SpriteComponent} which repeats the sprite.
 * @author Sam
 *
 */
public class TiledSpriteComponent extends SpriteComponent {
	
	protected int tilesH, tilesV,
				tileW, tileH;

	public TiledSpriteComponent(TextureRegion textureRegion, int tilesH, int tilesV) {
		super(textureRegion);
		this.tilesH = tilesH;
		this.tilesV = tilesV;
		this.tileW = textureRegion.getRegionWidth();
		this.tileH = textureRegion.getRegionHeight();
	}
	
	@Override
	public void render(float delta) {
		for (int x=0; x<tilesH; x++) {
			for (int y=0; y<tilesV; y++) {
				drawAt(x*tileW, y*tileH);
			}
		}
	}
}
