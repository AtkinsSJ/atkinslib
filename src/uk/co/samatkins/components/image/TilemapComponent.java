package uk.co.samatkins.components.image;

import java.lang.reflect.Array;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.badlogic.gdx.utils.JsonValue;
import uk.co.samatkins.Entity;
import uk.co.samatkins.SortedList;
import uk.co.samatkins.components.AbstractComponent;
import uk.co.samatkins.components.Node;
import uk.co.samatkins.components.geom.PositionComponent;
import uk.co.samatkins.exceptions.NoValidPathException;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.OrderedMap;

public class TilemapComponent<Tile extends TilemapComponent.ITile>
		extends AbstractComponent {
	
	public interface ITile {
		public int getTileWidth();
		public int getTileHeight();
		public TextureRegion getTextureRegion();
	}
	
	private int tileWidth, tileHeight;
	private int tilesH, tilesV;
	private Tile[][] tiles;
	private boolean[][] solidTiles;
	
	private int zIndex = 0;
	private PositionComponent positionComponent;

	@SuppressWarnings("unchecked")
	public TilemapComponent(Class<? extends TilemapComponent.ITile> tileClass, int tileWidth, int tileHeight, int tilesH, int tilesV) {
		this.tiles = (Tile[][]) Array.newInstance(tileClass, tilesH, tilesV);
		this.solidTiles = new boolean[tilesH][tilesV];
		
		this.tileWidth = tileWidth;
		this.tileHeight = tileHeight;
		this.tilesH = tilesH;
		this.tilesV = tilesV;
	}
	
	@Override
	public void added(Entity entity) {
		super.added(entity);
		positionComponent = require(PositionComponent.class);
	}

	@Override
	public void render(float delta) {
		Vector2 position = positionComponent.getPosition();
		
		float cameraBottom = getScene().getCameraBottom(),
				cameraLeft = getScene().getCameraLeft(),
				cameraTop = getScene().getCameraTop(),
				cameraRight = getScene().getCameraRight();
		
		
		int tileXStart = (int) Math.floor( (cameraLeft - position.x) / tileWidth );
			if (tileXStart < 0) { tileXStart = 0; }
		int tileXEnd = (int) Math.ceil( (cameraRight - position.x) / tileWidth ) + 1;
			if (tileXEnd > tilesH) { tileXEnd = tilesH; }
			
		int tileYStart = (int) Math.floor( (cameraBottom - position.y) / tileHeight );
			if (tileYStart < 0) { tileYStart = 0; }
		int tileYEnd = (int) Math.ceil( (cameraTop - position.y) / tileHeight ) + 1;
			if (tileYEnd > tilesV) { tileYEnd = tilesV; }
		
		Tile tile;
		for (int tileX=tileXStart; tileX<tileXEnd; tileX++) {
			for (int tileY=tileYStart; tileY<tileYEnd; tileY++) {
				
				tile = tiles[tileX][tileY];
				if (tile != null) {
					getScene().drawSprite(
						tile.getTextureRegion(),
						position.x + (tileX * tile.getTileWidth()),
						position.y + (tileY * tile.getTileHeight()),
						zIndex
					);
				}
				
			}
		}
		
	}
	
	public void setZIndex(int zIndex) {
		this.zIndex = zIndex;
	}
	
	/**
	 * Is the given coordinate within the tilemap?
	 * @param x
	 * @param y
	 * @return
	 */
	private boolean tileExists(int x, int y) {
		return (x >= 0 && x < tilesH && y >= 0 && y < tilesV);
	}

	/**
	 * Return the tile at the given coordinate.
	 * 
	 * returns null if the coordinate is invalid or there is no tile at that position.
	 * @param x
	 * @param y
	 * @return
	 */
	public Tile getTile(int x, int y) {
		return tileExists(x,y) ? tiles[x][y] : null;
	}
	
	/**
	 * Assign the given tile to the given coordinate, if the coordinate is valid.
	 * @param x
	 * @param y
	 * @param tile
	 */
	public void setTile(int x, int y, Tile tile) {
		if (tileExists(x,y)) {
			tiles[x][y] = tile;
		}
	}
	
	/**
	 * Set the solid state of the tile at the given coordinate
	 * @param x
	 * @param y
	 * @param solid
	 */
	public void setTileSolid(int x, int y, boolean solid) {
		if (tileExists(x,y)) {
			solidTiles[x][y] = solid;
		}
	}
	
	/**
	 * Returns the solid state of the tile at the given coordinate.
	 * Returns true for invalid coordinates.
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isTileSolid(int x, int y) {
		if (tileExists(x,y)) {
			return solidTiles[x][y];
		}
		return true;
	}
	
	/**
	 * Set the solid state of a rectangle of tiles.
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param solid
	 */
	public void setRectSolid(int x, int y, int width, int height, boolean solid) {
		for (int x1=0; x1<width; x1++) {
			for (int y1=0; y1<height; y1++) {
				setTileSolid(x+x1, y+y1, solid);
			}
		}
	}
	
	/**
	 * Returns whether the given rectangle of the tilemap contains any solid tiles.
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @return
	 */
	public boolean rectContainsSolid(int x, int y, int width, int height) {
		for (int x1=0; x1<width; x1++) {
			for (int y1=0; y1<height; y1++) {
				if (isTileSolid(x+x1, y+y1)) return true;
			}
		}
		
		return false;
	}

	/**
	 * Get the coordinates for the tile at the given stage position.
	 * Caution: No checks are done to ensure the tile exists!
	 * @param stageCoordinates
	 * @return
	 */
	public Vector2 getTileCoordinatesAt(Vector2 stageCoordinates) {
		// To get grid position, subtract tilemap position from desired stage position, then divide by the tile size
		return stageCoordinates.cpy()
					.sub(positionComponent.getPosition())
					.sub(stageCoordinates.x % tileWidth, stageCoordinates.y % tileHeight)
					.div(tileWidth, tileHeight);
	}
	
	/**
	 * Get the stage position of a tile, based on its coordinates within the tilemap
	 * @param tileCoordinates
	 * @return
	 */
	public Vector2 getPositionOfTile(Vector2 tileCoordinates) {
		return tileCoordinates.cpy()
					.mul(tileWidth, tileHeight)
					.add(positionComponent.getPosition());
	}

	public int getWidth() {
		return tileWidth * tilesH;
	}

	public int getHeight() {
		return tileHeight * tilesV;
	}

	/**
	 * Generate a path from start to end, and return it as a list of tile coordinates.
	 * @param startX
	 * @param startY
	 * @param endX
	 * @param endY
	 * @return A list of tile coordinates as Vector2s.
	 * @throws NoValidPathException if no path can be found.
	 */
	public List<Vector2> findPath(int startX, int startY, int endX, int endY) throws NoValidPathException {
		
		/*
		 * TODO: An alternative pathfinder, where you specify a target area of tiles rather than just one.
		 */
		
		SortedList<Node> open = new SortedList<Node>();
		Set<Node> closed = new HashSet<Node>();
		
		// Generate empty grid
		Node[][] nodes = new Node[tilesH][tilesV];
		
		// Add the start node
		Node start = new Node(startX, startY, endX, endY, null);
		open.add(start);
		nodes[start.x][start.y] = start;
		
		Node current;
		
		while (true) {
			current = open.first();
			if (current == null) {
				throw new NoValidPathException();
			}
			
			// Did we get to the end?
			if (current.h == 0) {
				// Generate the path
				List<Vector2> path = new LinkedList<Vector2>();
				
				while (current != null) {
					path.add(0, new Vector2(current.x, current.y));
					current = current.parent;
				}
				
				return path;
			}
			
			/*
			 * Only be blocked by solid tiles that aren't start or end.
			 */
			if (isTileSolid(current.x, current.y) && current.g() != 0) {
				open.remove(current);
				closed.add(current);
				continue;
			}
			
			// Create nodes neighbouring current if they don't exist
			// Left
			if (current.x > 0) {
				createNode(current.x-1, current.y, endX, endY, current, open, closed, nodes);
			}
			
			// Right
			if (current.x < (tilesH-1)) {
				createNode(current.x+1, current.y, endX, endY, current, open, closed, nodes);
			}
			
			// Up
			if (current.y < (tilesV-1)) {
				createNode(current.x, current.y+1, endX, endY, current, open, closed, nodes);
			}
			
			// Down
			if (current.y > 0) {
				createNode(current.x, current.y-1, endX, endY, current, open, closed, nodes);
			}
			
			open.remove(current);
			closed.add(current);
		}
	}
	
	private void createNode(int x, int y, int endX, int endY, Node parent, SortedList<Node> open, Set<Node> closed, Node[][] nodes) {
		Node newNode = new Node( x, y, endX, endY, parent);
		
		if (nodes[newNode.x][newNode.y] == null) {
			nodes[newNode.x][newNode.y] = newNode;
			open.add(newNode);
		} else if (newNode.g() < nodes[newNode.x][newNode.y].g()) {
			nodes[newNode.x][newNode.y].set(newNode.parent);
		}
	}

	public void highlightPath(List<Vector2> path, Tile tile) {
		for (Vector2 p: path) {
			setTile( (int) p.x, (int) p.y, tile);
		}
	}

	@Override
	public void write(Json json) {
		super.write(json);
		// TODO: Implement writing to Json
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		super.read(json, jsonData);
		
		// TODO: Implement loading from Json
	}
}
