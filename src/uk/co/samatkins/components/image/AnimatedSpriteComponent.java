package uk.co.samatkins.components.image;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimatedSpriteComponent extends SpriteComponent {
	
	private TextureRegion[][] frames;
	private Map<String, Animation> animations;
	private Animation currentAnimation;

	public AnimatedSpriteComponent(TextureRegion textureRegion,
			int frameWidth, int frameHeight) {
		this(textureRegion, frameWidth, frameHeight, false, false);
	}
	
	public AnimatedSpriteComponent(TextureRegion textureRegion,
			int frameWidth, int frameHeight,
			boolean centreX, boolean centreY) {
		this(textureRegion, frameWidth, frameHeight,
				centreX ? frameWidth/-2 : 0, centreY ? frameHeight/-2 : 0);
	}
	
	public AnimatedSpriteComponent(TextureRegion textureRegion,
			int frameWidth, int frameHeight,
			float offsetX, float offsetY) {
		super(textureRegion);
		
		/* FIX: If the texture region is already flipped when we generate
		 * the frames, it produces nonsense.
		 */
		setFlipped(false, false);

		int across = textureRegion.getRegionWidth() / frameWidth;
		int down = textureRegion.getRegionHeight() / frameHeight;
		
		frames = new TextureRegion[across][down];
		for (int x=0; x<across; x++) {
			for (int y=0; y<down; y++) {
				frames[x][y] = new TextureRegion(
						textureRegion, x*frameWidth, y*frameWidth,
						frameWidth, frameHeight);
			}
		}
		
		animations = new HashMap<String, Animation>();
		
		setOffset(offsetX, offsetY);
	}
	
	@Override
	public void render(float delta) {
		if (currentAnimation != null) {
			currentAnimation.update(delta);
			setTextureRegion(getFrame(currentAnimation.getCurrentFrame()));
		}
		
		super.render(delta);
	}
	
	/**
	 * Creates a new animation
	 * @param name
	 * @param frames
	 * @param framesPerSecond
	 * @param loops
	 */
	public void addAnimation(String name, int[][] frames,
			float framesPerSecond, boolean loops) {
		Coord[] frameCoords = new Coord[frames.length];
		
		for (int i=0; i<frames.length; i++) {
			frameCoords[i] = new Coord(frames[i][0], frames[i][1]);
		}
		
		animations.put(name, new Animation(name, frameCoords, framesPerSecond, loops));
	}
	
	/**
	 * Play an animation, as created by {@link #addAnimation(String, int[][], float, boolean)}
	 * @param name
	 * @param restart
	 */
	public void playAnimation(String name, boolean restart) {
		if (restart || currentAnimation == null || !currentAnimation.name.equals(name)) {
			startAnimation(name);
		}
	}
	public void playAnimation(String name) {
		playAnimation(name, false);
	}
	
	/**
	 * Internal function, start playing an animation from the beginning
	 * @param name
	 */
	private void startAnimation(String name) {
		currentAnimation = animations.get(name);
		currentAnimation.start();
	}
	
	public String getCurrentAnimationName() {
		if (currentAnimation == null) {
			return null;
		}
		return currentAnimation.name;
	}
	
	public TextureRegion getFrame(Coord position) {
		return frames[position.x][position.y];
	}
	
	private class Coord {
		int x, y;
		public Coord(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
	
	public class Animation {
		String name;
		Coord[] frames;
		float timePerFrame;
		boolean loops;
		
		private int currentFrame = 0;
		private float counter = 0;
		
		public Animation(String name, Coord[] frames, float framesPerSecond, boolean loops) {
			this.name = name;
			this.frames = frames;
			this.timePerFrame = 1f / framesPerSecond;
			this.loops = loops;
		}
		
		public void start() {
			currentFrame = 0;
			counter = 0;
		}
		
		public void update(float delta) {
			counter += delta;
			while (counter > timePerFrame) {
				counter -= timePerFrame;
				
				if (currentFrame >= frames.length-1) {
					if (loops) {
						currentFrame = 0;
					}
				} else {
					currentFrame++;
				}
			}
		}
		
		public Coord getCurrentFrame() {
			return frames[currentFrame];
		}
	}
}
