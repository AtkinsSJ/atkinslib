package uk.co.samatkins.components.image;

import uk.co.samatkins.Entity;
import uk.co.samatkins.components.AbstractComponent;
import uk.co.samatkins.components.geom.PositionComponent;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class SpriteComponent extends AbstractComponent {
	
	private PositionComponent positionComponent;
	
	private TextureRegion textureRegion;
	private Color color = Color.WHITE,
			temporaryColor = null;
	
	private Vector2 offset;
	private int zIndex = 0;
	private float rotation = 0;
	private float scale = 1f;
	
	private boolean isFlippedX, isFlippedY;

	public SpriteComponent(TextureRegion textureRegion) {
		this(textureRegion, Vector2.Zero);
	}
	
	public SpriteComponent(TextureRegion textureRegion, boolean centreX, boolean centreY) {
		this(textureRegion);
		Vector2 centreOffset = getCentreOffset(textureRegion.getRegionWidth(), textureRegion.getRegionHeight());
		if (!centreX) centreOffset.x = 0;
		if (!centreY) centreOffset.y = 0;
		setOffset(centreOffset);
	}
	
	// This constructor is called by all others
	public SpriteComponent(TextureRegion textureRegion, Vector2 offset) {
		this.textureRegion = textureRegion;
		this.offset = new Vector2(offset);
		this.isFlippedX = this.isFlippedY = false;
	}
	
	@Override
	public void added(Entity entity) {
		super.added(entity);
		positionComponent = require(PositionComponent.class);
	}

	@Override
	public void render(float delta) {
		drawAt(0, 0);
	}
	
	/**
	 * Draw the current textureRegion at the entity's position, plus the offset, plus the given position.
	 * @param x
	 * @param y
	 */
	protected void drawAt(float x, float y) {
		getScene().drawSprite(
				textureRegion,
				positionComponent.getX() + offset.x + x,
				positionComponent.getY() + offset.y + y,
				zIndex,
				temporaryColor == null ? color : temporaryColor,
				rotation,
				scale
		);
		temporaryColor = null;
	}
	
	/**
	 * Assign a z-index. The higher, the closer to the camera.
	 * @param zIndex
	 */
	public void setZIndex(int zIndex) {
		this.zIndex = zIndex;
	}

	public void setOffset(float x, float y) {
		this.offset.set(x, y);
	}

	public void setOffset(Vector2 offset) {
		setOffset(offset.x, offset.y);
	}
	
	public Vector2 getCentreOffset(float width, float height) {
		return new Vector2(width / -2, height / -2);
	}
	
	public void setTextureRegion(TextureRegion textureRegion) {
		this.textureRegion = textureRegion;

		boolean wasFlippedX = textureRegion.isFlipX();
		boolean wasFlippedY = textureRegion.isFlipY();
		this.textureRegion.flip(wasFlippedX != isFlippedX , wasFlippedY != isFlippedY);
	}
	
	public void setFlipped(boolean flipX, boolean flipY) {
		boolean wasFlippedX = textureRegion.isFlipX();
		boolean wasFlippedY = textureRegion.isFlipY();
		
		this.textureRegion.flip(wasFlippedX != flipX, wasFlippedY != flipY);
		
		this.isFlippedX = flipX;
		this.isFlippedY = flipY;
	}
	
	public boolean isFlippedX() {
		return this.isFlippedX;
	}
	public boolean isFlippedY() {
		return this.isFlippedY;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	/**
	 * Set the rotation of the sprite, in degrees.
	 * @param rotation
	 */
	public void setRotation(float rotation) {
		this.rotation = rotation;
	}
	
	/**
	 * Sets the color for a single frame, after which it reverts to what it was before.
	 * @param color
	 */
	public void setTemporaryColor(Color color) {
		this.temporaryColor = color;
	}
	
	public void setScale(float scale) {
		this.scale = scale;
	}
	
	public float getScale() {
		return scale;
	}
}
