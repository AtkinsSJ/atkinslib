package uk.co.samatkins.components;


/**
 * Component that can execute code after a given delay.
 * @author Sam
 *
 */
public class CountdownComponent extends AbstractComponent {
	
	public interface CountdownCallback {
		public void onCountdownComplete();
	}
	
	private boolean running;
	private float time;
	private CountdownCallback callback;

	public CountdownComponent() {
		running = false;
		time = 0;
	}
	
	/**
	 * Start counting down from time seconds,
	 * after which callback.onCountdownComplete() will be run.
	 * @param time
	 * @param callback
	 */
	public void startCountdown(float time, CountdownCallback callback) {
		this.time = time;
		this.callback = callback;
		running = true;
	}
	
	/**
	 * @return The number of seconds remaining.
	 */
	public float getTimeLeft() {
		return time;
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
		
		if (running) {
			time -= delta;
			if (time < 0) {
				time = 0;
				running = false;
				callback.onCountdownComplete();
			}
		}
		
	}

}
