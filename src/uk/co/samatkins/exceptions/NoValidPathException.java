package uk.co.samatkins.exceptions;

import uk.co.samatkins.components.image.TilemapComponent;

/**
 * Thrown by {@link TilemapComponent#findPath(int, int, int, int)}
 * when there is no possible path.
 * @author Sam
 *
 */
public class NoValidPathException extends Exception {

	private static final long serialVersionUID = -3431567860176540540L;

}