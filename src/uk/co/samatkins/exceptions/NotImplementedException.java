package uk.co.samatkins.exceptions;

public class NotImplementedException extends RuntimeException {

	private static final long serialVersionUID = 5551991039734084749L;

	public NotImplementedException(String message) {
		super(message);
	}

	public NotImplementedException(Throwable cause) {
		super(cause);
	}

	public NotImplementedException(String message, Throwable cause) {
		super(message, cause);
	}

}
