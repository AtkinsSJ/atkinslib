package uk.co.samatkins;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;


public class Game extends com.badlogic.gdx.Game {

	private Skin skin;
	private Scene<? extends Game> scene;

	@Override
	public void create() {
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("packed.atlas"));
		skin = new Skin(Gdx.files.internal("skin.json"), atlas);
	}

	public void setScene(Scene<? extends Game> scene) {
		this.scene = scene;
		setScreen(scene);
	}
	
	public Scene<? extends Game> getScene() {
		return scene;
	}
	
	public Skin getSkin() {
		return skin;
	}

}
