package uk.co.samatkins;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class SortedListTest {
	private SortedList<Integer> list;
	
	@Before 
	public void setup() {
		list = new SortedList<Integer>();
	}

	@Test
	public void testSortedList() {
		assertEquals("List starts-off empty", 0, list.size());
	}

	@Test
	public void testIterator() {
		Integer first = 5,
				second = 10,
				third = 15;
		
		list.add(second);
		list.add(third);
		list.add(first);
		
		Iterator<Integer> it = list.iterator();
		assertNotNull("iterator isn't null", it);
		
		assertTrue("Iterator has a first item", it.hasNext());
		assertEquals("First next() returns first element", first, it.next());
		
		assertTrue("Iterator has a second item", it.hasNext());
		assertEquals("Second next() returns second element", second, it.next());
		
		it.remove();
		
		assertTrue("Iterator has a third item, after remove()", it.hasNext());
		assertEquals("Third next() returns third element, after remove()", third, it.next());
		
		assertEquals("remove() removed an element", 2, list.size());
		
	}

	@Test
	public void testAdd() {
		int initialSize = list.size();
		list.add(100);
		assertEquals("add() increases the size by 1", initialSize + 1, list.size());
		
		list.add(100);
		assertEquals("Adding the same element twice will keep both.", initialSize + 2, list.size());
	}

	@Test
	public void testAddAll() {
		int initialSize = list.size();
		
		Set<Integer> set = new HashSet<Integer>();
		set.add(100);
		set.add(200);
		set.add(150);
		
		int totalSize = initialSize + set.size();
		list.addAll(set);
		assertEquals("addAll() increases the size by the size of the given collection",
				totalSize, list.size());
		assertTrue("addAll() actually adds all of the elements.", list.containsAll(set));
	}

	@Test
	public void testClear() {
		list.add(100);
		list.add(100);
		list.add(50);
		
		list.clear();
		assertEquals("clear() removes all elements and sets size to 0.", 0, list.size());
		
		list.clear();
		assertEquals("Calling clear() when already empty doesn't cause problems.", 0, list.size());
	}

	@Test
	public void testContains() {
		Integer wanted = 100;
		list.add(200);
		list.add(50);
		
		assertFalse("contains() returns false when the element isn't in the list.",
				list.contains(wanted));
		
		list.add(wanted);
		
		assertTrue("contains() returns true when the element is in the list.",
				list.contains(wanted));
	}

	@Test
	public void testContainsAll() {
		Integer a = 1,
				b = 2,
				c = 3,
				d = 4;
		list.add(a);
		list.add(b);
		list.add(c);
		
		Set<Integer> set = new HashSet<Integer>();
		set.add(a);
		set.add(b);
		set.add(c);
		
		assertTrue("containsAll true if the elements are in both collections.",
				list.containsAll(set));
		
		list.add(d);
		assertTrue("containsAll true even if the list has elements the other collection doesn't.",
				list.containsAll(set));
		
		list.remove(b);
		assertFalse("containsAll false if an element in the collection isn't in the list.",
				list.containsAll(set));
	}

	@Test
	public void testIsEmpty() {
		assertTrue("Size is 0 initially, so is empty", list.isEmpty());
		list.add(100);
		assertFalse("After adding an element, list no longer empty.", list.isEmpty());
		list.remove(100);
		assertTrue("After removing the element again, list is empty again.", list.isEmpty());
	}

	@Test
	public void testRemove() {
		Integer removed = 100;
		list.add(10);
		list.add(removed);
		list.add(50);
		
		assertTrue("remove() returns true when the given element was removed.",
				list.remove(removed));
		assertEquals("remove() decreases the list size when successful", 2, list.size());
		
		assertFalse("remove() returns false if the element wasn't found.",
				list.remove(removed));
		assertEquals("remove() doesn't decrease the list size when unsuccessful", 2, list.size());
		
		list.add(removed);
		list.add(removed);

		assertTrue("remove() returns true when the given element was removed a single time",
				list.remove(removed));
		assertEquals("remove() only removes the element once, even if it existed multiple times in the list",
				3, list.size());
		
	}

	@Test
	public void testRemoveAll() {
		Integer a = 1,
				b = 2,
				c = 3,
				d = 4;
		list.add(a);
		list.add(b);
		list.add(c);
		list.add(c);
		list.add(d);
		
		Set<Integer> set = new HashSet<Integer>();
		set.add(a);
		set.add(b);
		set.add(c);
		
		boolean changed = list.removeAll(set);
		assertEquals("removeAll() removes any items that are in the collection, including duplicates.",
				1, list.size());
		assertTrue("removeAll() returns true when items are removed.", changed);
		
		changed = list.removeAll(set);
		assertFalse("removeAll() returns false when the list isn't changed.", changed);
	}

	@Test
	public void testRetainAll() {
		Integer a = 1,
				b = 2,
				c = 3,
				d = 4;
		list.add(a);
		list.add(b);
		list.add(c);
		list.add(c);
		list.add(d);
		
		Set<Integer> set = new HashSet<Integer>();
		set.add(a);
		set.add(b);
		set.add(c);
		
		boolean changed = list.retainAll(set);
		assertEquals("retainAll() removes any items that are not in the collection, including duplicates.",
				4, list.size());
		assertTrue("retainAll() returns true when items are removed.", changed);
		
		changed = list.retainAll(set);
		assertFalse("retainAll() returns false when the list isn't changed.", changed);
	}

	@Test
	public void testSize() {
		assertEquals("Size is 0 initially", 0, list.size());
		list.add(1);
		assertEquals("Size is number of elements.", 1, list.size());
	}

	@Test
	public void testToArray() {
		list.add(5);
		list.add(3);
		list.add(1);
		list.add(Integer.MAX_VALUE);
		list.add(2);
		list.add(4);
		
		Integer[] array = new Integer[] {
			1,2,3,4,5,Integer.MAX_VALUE
		};
		
		assertArrayEquals("toArray returns an array in sorted order.",
				array, list.toArray());
	}

	@Test
	public void testToArrayTArray() {
		list.add(5);
		list.add(3);
		list.add(1);
		list.add(2);
		list.add(4);
		
		Integer[] array = new Integer[] {
			1,2,3,4,5
		};
		Integer[] resultArray = new Integer[5];
		resultArray = list.toArray(resultArray);
		
		assertArrayEquals("toArray(Array) returns an array in sorted order.",
				array, resultArray);
	}

}
