package uk.co.samatkins;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

/**
 * Input listener for testing. Stores the input it's given.
 * @author Sam
 *
 */
public class MockInputListener extends InputListener {
	
	private boolean handleEvents;
	
	public Vector2 lastEnter,
					lastExit,
					lastMove,
					lastTouchDown,
					lastTouchUp,
					lastTouchDragged;

	public int lastKeyDown,
				lastKeyUp,
				lastScrollAmount;
	
	public char lastKeyTyped;
	
	public MockInputListener(boolean handleEvents) {
		this.handleEvents = handleEvents;
	}
	
	@Override
	public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
		lastEnter = new Vector2(x, y);
		if (handleEvents) event.handle();
	}
	
	@Override
	public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
		lastExit = new Vector2(x,y);
		if (handleEvents) event.handle();
	}
	
	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		lastKeyDown = keycode;
		return handleEvents;
	}
	
	@Override
	public boolean keyUp(InputEvent event, int keycode) {
		lastKeyUp = keycode;
		return handleEvents;
	}
	
	@Override
	public boolean keyTyped(InputEvent event, char character) {
		lastKeyTyped = character;
		return handleEvents;
	}
	
	@Override
	public boolean mouseMoved(InputEvent event, float x, float y) {
		lastMove = new Vector2(x,y);
		return handleEvents;
	}
	
	@Override
	public boolean scrolled(InputEvent event, float x, float y, int amount) {
		lastScrollAmount = amount;
		return handleEvents;
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		lastTouchDown = new Vector2(x,y);
		return handleEvents;
	}
	
	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
		lastTouchUp = new Vector2(x,y);
		if (handleEvents) event.handle();
	}
	
	@Override
	public void touchDragged(InputEvent event, float x, float y, int pointer) {
		lastTouchDragged = new Vector2(x,y);
		if (handleEvents) event.handle();
	}

}
