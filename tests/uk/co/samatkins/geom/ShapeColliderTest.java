package uk.co.samatkins.geom;

import static org.junit.Assert.*;
import junit.framework.Assert;
import static uk.co.samatkins.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;

public class ShapeColliderTest {
	


	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void doesCircleIntersectCircle() {
		Circle a = new Circle(0,0,10);
		Circle b = new Circle(10,0,10);

		assertTrue(ShapeCollider.doesCircleIntersectCircle(a, b));
		
		b.setCentre(25, 0);
		assertFalse(ShapeCollider.doesCircleIntersectCircle(a, b));
	}
	
	@Test
	public void doesCircleIntersectRectangle() {
		Circle a = new Circle(0,0, 10);
		Rectangle b = new Rectangle(5, 0, 10, 10);
		
		assertTrue(ShapeCollider.doesCircleIntersectRectangle(a, b));
		
		b.setLeft(15);
		assertFalse(ShapeCollider.doesCircleIntersectRectangle(a, b));
	}
	
	@Test
	public void doesRectangleIntersectRectangle() {
		Rectangle a = new Rectangle(0,0, 10,10);
		Rectangle b = new Rectangle(10,0, 10,10);
		
		assertTrue(ShapeCollider.doesRectangleIntersectRectangle(a, b));
		
		b.setLeft(20);
		assertFalse(ShapeCollider.doesRectangleIntersectRectangle(a, b));
	}
	
	@Test
	public void doesCircleIntersectRATriangle() {
		RATriangle b = new RATriangle(0,0, 10,10, true,true);
		Circle a = new Circle(10,0, 10);
		
		assertTrue(ShapeCollider.doesCircleIntersectRATriangle(a, b));
		
		a.setCentre(15,0);
		assertFalse(ShapeCollider.doesCircleIntersectRATriangle(a, b));
	}
	
	@Test
	public void doesRectangleIntersectRATriangle() {
		Rectangle a = new Rectangle(0,0, 10,10);
		RATriangle b = new RATriangle(0,0, 10,10, false,false);
		
		assertTrue(ShapeCollider.doesRectangleIntersectRATriangle(a, b));
		
		b.setLeft(5);
		b.setBottom(-5);
		assertFalse(ShapeCollider.doesRectangleIntersectRATriangle(a, b));
	}
	
	@Test
	public void doesRATriangleIntersectRATriangle() {
		RATriangle a = new RATriangle(0,0, 10,10, true,true);
		RATriangle b = new RATriangle(-1,0, 10,10, false,false);
		
		assertTrue(ShapeCollider.doesRATriangleIntersectRATriangle(a, b));
		
		b.setLeft(1);
		assertFalse(ShapeCollider.doesRATriangleIntersectRATriangle(a, b));
	}
	
	@Test
	public void calculateExitVectorCC() {
		Circle stationary = new Circle(0,0, 10);
		Circle circle = new Circle(15,0, 10);
		
		assertEquals(new Vector2(5,0),
				ShapeCollider.calculateExitVectorCC(circle, stationary));
		
		circle.setCentre(0, 15);
		assertEquals(new Vector2(0,5),
				ShapeCollider.calculateExitVectorCC(circle, stationary));
		
//		circle.setCentre(100, 100);
//		assertEquals("Returns Vector2.Zero when there is no intersection.",
//				Vector2.Zero,
//				ShapeCollider.calculateExitVectorCC(circle, stationary));
	}
	
	@Test
	public void calculateExitVectorRR() {
		Rectangle stationary = new Rectangle(0, 0, 10, 10);
		Rectangle rect = new Rectangle(5, 0, 10, 10);
		
		assertEquals(new Vector2(5, 0),
				ShapeCollider.calculateExitVectorRR(rect, stationary));
		
//		rect.setCentre(100, 100);
//		assertEquals("Returns Vector2.Zero when there is no intersection.",
//				Vector2.Zero,
//				ShapeCollider.calculateExitVectorRR(rect, stationary));
	}
	
	@Test
	public void calculateExitVectorCR() {
		Rectangle rect = new Rectangle(-5, -5, 10, 10);
		Circle circle = new Circle(10, 0, 5);
		
		for (int i=1; i<=5; i++) {
			circle.setCentre(10-i, 0);
			assertEquals(new Vector2(i, 0),
				ShapeCollider.calculateExitVectorCR(circle, rect));
		}
		
//		circle.setCentre(100, 100);
//		assertEquals("Returns Vector2.Zero when there is no intersection.",
//				Vector2.Zero,
//				ShapeCollider.calculateExitVectorCR(circle, rect));
	}
	
	@Test
	public void calculateExitVectorRRat() {
		fail("Not implemented");
	}
	
	@Test
	public void calculateExitVectorCRat() {
		RATriangle tri = new RATriangle(0,0, 10,10, true,true);
		Circle circle = new Circle(-5,5,10);
		
		assertEquals(new Vector2(-5, 0),
				ShapeCollider.calculateExitVectorCRat(circle, tri));
		
		circle.setCentre(5, 15);
		assertEquals(new Vector2(0,5),
				ShapeCollider.calculateExitVectorCRat(circle, tri));
		
		circle.setCentre(10, 0);
		Vector2 result = new Vector2(10,0).rotate(-45).sub(5,-5);
		assertEquals(result,
				ShapeCollider.calculateExitVectorCRat(circle, tri));
	}
	
	@Test
	public void calculateExitVectorRatRat() {
		fail("Not implemented");
	}
	
	@Test
	public void calculateExitVectorCirclePoint() {
		Vector2 point = new Vector2(0,0);
		Circle circle = new Circle(5, 0, 10);
		
		assertEquals(new Vector2(5,0), 
				ShapeCollider.calculateExitVectorCirclePoint(circle, point));
		
//		circle.setCentre(100, 100);
//		assertEquals("Returns Vector2.Zero when there is no intersection.",
//				Vector2.Zero,
//				ShapeCollider.calculateExitVectorCirclePoint(circle, point));
	}

	@Test
	public void isInLeftHalfSpace() {
		Vector2 start = new Vector2(0,0),
				end = new Vector2(10,10),
				point = new Vector2(0,5);
		
		assertTrue("0,5 is left of the line.", ShapeCollider.isInLeftHalfSpace(start, end, point));
		
		point.set(5, 10);
		assertTrue("5,10 is left of the line.", ShapeCollider.isInLeftHalfSpace(start, end, point));
		
		point.set(5,0);
		assertFalse("5,0 is right of the line.", ShapeCollider.isInLeftHalfSpace(start, end, point));
		
		point.set(10, 5);
		assertFalse("10,5 is right of the line.", ShapeCollider.isInLeftHalfSpace(start, end, point));
		
		point.set(5, 5);
		assertTrue("5,5 is on the line.", ShapeCollider.isInLeftHalfSpace(start, end, point));
	}
	
	@Test
	public void component2d() {
		Assert.assertEquals(Math.sqrt(2), 
				ShapeCollider.component2d(new Vector2(2,0).rotate(45), new Vector2(1,0)), 0.01f);
	}

}
