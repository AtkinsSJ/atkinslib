package uk.co.samatkins.geom;

import static org.junit.Assert.*;

import org.junit.Test;

import com.badlogic.gdx.math.Vector2;

public class RectangleTest {

	@Test
	public void test() {
		Rectangle r = new Rectangle(0, 0, 10, 8);
		Vector2 centre = new Vector2(5, 4);
		
		assertEquals("Rectangle returns correct centre", centre, r.getCentre());
		
		assertEquals("Rectangle's width", 10, r.getWidth(), 0.01f);
		assertEquals("Rectangle's height", 8, r.getHeight(), 0.01f);
		
		assertEquals("Rectangle's left", 0, r.getLeft(), 0.01f);
		assertEquals("Rectangle's right", 10, r.getRight(), 0.01f);
		assertEquals("Rectangle's top", 8, r.getTop(), 0.01f);
		assertEquals("Rectangle's bottom", 0, r.getBottom(), 0.01f);
	}

}
