package uk.co.samatkins.geom;

import static org.junit.Assert.*;
import static uk.co.samatkins.Assert.*;

import org.junit.Test;

import com.badlogic.gdx.math.Vector2;

public class LineTest {

	@Test
	public void testCalculateLeftNormal() {
		Line hl = new Line(0, 0, 100, 0);
		assertEquals(new Vector2(0,1), hl.calculateLeftNormal());
	}

}
