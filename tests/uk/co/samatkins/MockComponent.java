package uk.co.samatkins;

import uk.co.samatkins.components.AbstractComponent;

public class MockComponent extends AbstractComponent {
	
	public int timesUpdated = 0;
	public boolean addedCalled = false;
	public boolean addedToSceneCalled = false;
	public boolean removedFromSceneCalled = false;
	public Scene<?> scene;

	public MockComponent() {
		
	}

	@Override
	public void added(Entity entity) {
		super.added(entity);
		addedCalled = true;
	}
	
	@Override
	public void addedToScene(Scene<?> scene) {
		super.addedToScene(scene);
		addedToSceneCalled = true;
		this.scene = scene;
	}

	@Override
	public void update(float delta) {
		timesUpdated++;
	}

	@Override
	public void removedFromScene(Scene<?> scene) {
		removedFromSceneCalled = true;
	}
}
