package uk.co.samatkins;

import static junit.framework.Assert.format;

import com.badlogic.gdx.math.Vector2;

/**
 * Custom asserts, for use in JUnit tests
 * @author Sam
 *
 */
public class Assert extends org.junit.Assert {

	public static final float EPSILON = 0.01f;

	public static void assertEquals(Vector2 expected, Vector2 actual) {
		assertEquals(null, expected, actual, EPSILON);
	}
	
	public static void assertEquals(String message, Vector2 expected, Vector2 actual, float epsilon) {
		if (expected.epsilonEquals(actual, epsilon)) {
			return;
		}
		
		fail(format(message, expected, actual));
	}
}
