package uk.co.samatkins;

import uk.co.samatkins.Scene.ResizeListener;

public class MockResizeListener implements ResizeListener {

	public int width = 0, height = 0;

	@Override
	public void resize(int width, int height) {
		this.width = width;
		this.height = height;
	}

}
