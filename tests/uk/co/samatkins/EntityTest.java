package uk.co.samatkins;

import static org.junit.Assert.*;

import org.junit.*;

import uk.co.samatkins.components.IComponent;

public class EntityTest {
	
	protected static boolean addedCalled;
	
	@Test
	public void setScene() {
		Entity entity = new Entity();
		Scene<?> scene = SceneTest.newScene();
		
		entity.setScene(scene);
		
		assertEquals("setScene must set the entity's scene.", scene, entity.getScene());
	}
	
	@Test
	public void addedCallsEntityAddedOnComponents() {
		Entity e = new Entity();
		MockComponent m = new MockComponent();
		e.addComponent(m);
		
		e.setScene(SceneTest.newScene());
		e.added();
		assertEquals("added() calls addedToScene() on components.", true, m.addedToSceneCalled);
		assertNotNull("addedToScene() is called with a valid Scene.", m.scene);
	}
	
	@Test
	public void addComponent() {
		Entity e = new Entity();
		MockComponent c = new MockComponent();
		IComponent received = e.addComponent(c);
		
		assertEquals("addComponent() returns the component given", c, received);
		assertEquals("addComponent() adds the component", 1, e.getComponentCount());
		
		assertEquals("addComponent() calls added() on the component", true, c.addedCalled);
		
		e.addComponent(c);
		assertEquals("addComponent() must not allow duplicates to be added.", 1, e.getComponentCount());
	}
	
	@Test
	public void updateUpdatesComponents() {
		Entity e = new Entity();
		MockComponent c = new MockComponent();
		e.addComponent(c);
		e.update(0.1f);
		assertEquals("update() calls update() in components", 1, c.timesUpdated);
	}
	
	@Test
	public void getComponent() {
		Entity e = new Entity();
		MockComponent m = new MockComponent();
		e.addComponent(m);
		
		assertEquals("Can retrieve components from entities.", m, e.getComponent(MockComponent.class));
	}
	
	@Test
	public void entityHasTags() {
		String tag1 = "tag1",
				tag2 = "tag2",
				tag3 = "tag3",
				tag4 = "tag4";
		Entity e = new Entity(tag1);
		assertTrue("entity constructor assigns tag", e.hasTag(tag1));
		assertFalse("hasTag returns false when doesn't have tag", e.hasTag(tag2));
		
		e.addTag(tag2);
		assertTrue("addTag assigns tags", e.hasTag(tag2));
		
		Scene<?> s = SceneTest.newScene();
		s.addEntity(e);
		assertNotNull("Adding an entity to a scene adds the entity and tags to the ts map.",
				s.getEntityByTag(tag1));
		
		e.addTags(tag2, tag3, tag4);
		assertEquals("addTags does not duplicate tags", 4, e.getTags().size());
		assertTrue("addTags correctly assigns tags", e.hasTag(tag3) && e.hasTag(tag4));
		
		assertNotNull("Adding tags to an entity already in a scene adds them to the scene tag maps.",
				s.getEntityByTag(tag4));
		
		e.removeTag(tag4);
		assertFalse("removeTag removes the tag from the entity", e.hasTag(tag4));
		assertNull("Removing tags from an entity already in a scene removes them from the tag maps",
				s.getEntityByTag(tag4));
	}

	@Test
	public void onRemove() {
		Entity entity = new Entity();
		MockComponent mock = new MockComponent();
		entity.addComponent(mock);
		
		entity.removed();
		assertTrue("removed() calls removedFromScene() in components.", mock.addedCalled);
	}
}
