package uk.co.samatkins;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;

public class MockWidget extends Widget {
	
	private float delta = 0;
	private int timesDrawn = 0;

	public MockWidget() {
		
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		this.delta = delta;
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		timesDrawn++;
	}

	public float getDelta() {
		return this.delta;
	}
	
	public int getTimesDrawn() {
		return this.timesDrawn;
	}
}
