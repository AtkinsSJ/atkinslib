package uk.co.samatkins.components.geom;

import static org.junit.Assert.*;

import org.junit.*;

import uk.co.samatkins.components.geom.PositionComponent;

import com.badlogic.gdx.math.Vector2;

public class PositionComponentTest {

	@Test
	public void copiesPositionVector() {
		Vector2 position = new Vector2(1,2);
		PositionComponent c = new PositionComponent(position);
		assertNotSame("Should be a copy of the vector,  not the vector itself.", position, c.getPosition());
		assertEquals("Should have same value as the given vector", position, c.getPosition());
	}
	
	@Test
	public void canSetPosition() {
		PositionComponent c = new PositionComponent(new Vector2(100, 200));
		Vector2 pos = new Vector2(10, 20);
		c.setPosition(pos.x, pos.y);
		assertEquals("Can set a new position with setPosition()", pos, c.getPosition());
	}

	@Test
	public void moveBy() {
		Vector2 position = new Vector2(0,0);
		PositionComponent c = new PositionComponent(position);
		
		Vector2 startPos = c.getPosition();
		Vector2 movement = new Vector2(100,200);
		c.moveBy(movement);
		assertEquals("moveBy corrently moves relative to the current position",
				startPos.add(movement), c.getPosition());
	}
}
