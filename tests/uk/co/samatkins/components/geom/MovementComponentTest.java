package uk.co.samatkins.components.geom;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import uk.co.samatkins.Entity;
import uk.co.samatkins.components.geom.MovementComponent;
import uk.co.samatkins.components.geom.PositionComponent;

import com.badlogic.gdx.math.Vector2;

public class MovementComponentTest {
	
	Entity e;
	MovementComponent m;
	PositionComponent p;
	
	@Before
	public void setup() {
		e = new Entity();
		m = new MovementComponent();
		e.addComponent(m);
		p = e.getComponent(PositionComponent.class);
	}

	@Test
	public void holdsReferenceToPositionComponent() {
		assertNotNull("Holds reference to position component", m.positionComponent);
	}
	
	@Test
	public void holdsSpeed() {
		float speed = 5.0f;
		m.setSpeed(speed);
		assertEquals("MovementComponent holds speed", speed, m.getSpeed(), 0.01f);
	}
	
	@Test
	public void moveTowardsTarget() {
		m.setSpeed(50);
		// Make the target 100 units away
		Vector2 pos = new Vector2(100,0);
			pos.setAngle(45);
		m.setTargetPosition( pos );
		e.update(0.1f);
		// Distance should be 95 units now
		assertEquals("setTargetPosition causes movement toward the target",
				95f, p.getPosition().dst(pos), 0.01f);
		
		e.update(2);
		// Distance should be 0 units now
		assertEquals("Entity reaches the target",
				0f, p.getPosition().dst(pos), 0.01f);
		assertEquals("Once the target is reached, movement should stop",
				false, m.hasTarget());
	}
	
	@Test
	public void moveTowardsRelativeTarget() {

		Vector2 position = new Vector2(100,100),
				movement = new Vector2(-100,-100),
				destination = position.cpy().add(movement);
		p.setPosition(position);
		m.setSpeed(50);
		m.setRelativeTarget(movement);
		
		e.update(0.1f);
		
		// Distance should be smaller than movement's length
		assertEquals("setRelativeTarget causes movement towards the relative location",
				true, (p.getPosition().dst(destination)) < movement.len());
		
		e.update(5);
		// Distance should be 0 units now
		assertEquals("Entity reaches the target",
				0f, p.getPosition().dst(destination), 0.01f);
		assertEquals("once the target is reached, movement stops",
				false, m.hasTarget());
	}
	
	@Test
	public void cancelMovement() {
		Vector2 position = p.getPosition();
		m.setSpeed(50);
		m.setRelativeTarget(new Vector2(100,100));
		
		m.cancelMovement();
		
		e.update(0.1f);
		
		assertEquals("cancelMovement() prevents further movement towards target", position, p.getPosition());
		assertFalse("cancelMovement() causes the component to return false for hasTarget()", m.hasTarget());
	}

}
