package uk.co.samatkins.components;

import static org.junit.Assert.*;

import org.junit.Test;

import uk.co.samatkins.Entity;
import uk.co.samatkins.MockComponent;
import uk.co.samatkins.MockRequiredComponent;

public class AbstractComponentTest {

	@Test
	public void componentStoresEntityReference() {
		MockComponent a = new MockComponent();
		Entity e = new Entity();
		
		a.added(e);
		assertEquals("AbstractComponent keeps a reference to the entity.", e, a.getEntity());
	}
	
	@Test
	public void requireAddsComponentsToEntity() {
		Entity e = new Entity();
		MockComponent m = new MockComponent() {
			@Override
			public void added(Entity entity) {
				super.added(entity);
				require(MockRequiredComponent.class);
			}
		};
		e.addComponent(m);
		assertNotNull("Entity now contains a MockRequiredComponent",
				e.getComponent(MockRequiredComponent.class));
	}
	
	@Test
	public void requireReturnsTheComponent() {
		Entity e = new Entity();
		MockComponent m = new MockComponent() {
			@Override
			public void added(Entity entity) {
				super.added(entity);
				IComponent c = require(MockRequiredComponent.class);
				assertNotNull("require() returns the given component", c);
				assertTrue("require() returns component of the correct type.",
						c instanceof MockRequiredComponent);
			}
		};
		e.addComponent(m);
	}

}
