package uk.co.samatkins.components.image;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import uk.co.samatkins.Entity;
import uk.co.samatkins.MockGame;
import uk.co.samatkins.Scene;
import uk.co.samatkins.SceneTest;
import uk.co.samatkins.Sprite;
import uk.co.samatkins.components.geom.PositionComponent;
import uk.co.samatkins.components.image.SpriteComponent;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class SpriteComponentTest {
	
	Scene<MockGame> scene;
	Entity entity;
	SpriteComponent spriteComponent;
	PositionComponent positionComponent;
	
	int zIndex = 30;
	
	@Before
	public void before() {
		scene = SceneTest.newScene();
		entity = new Entity();
		Vector2 position = new Vector2(20, 30);
		positionComponent = new PositionComponent(position);
		spriteComponent = new SpriteComponent(new TextureRegion());
		spriteComponent.setZIndex(zIndex);
		
		entity.addComponent(positionComponent);
		entity.addComponent(spriteComponent);
		scene.addEntity(entity);
	}

	@Test
	public void drawsSprite() {
		entity.update(0.1f);
		assertEquals("SpriteComponent draws a sprite", 1, scene.getSpriteCount());
		
		Sprite sprite = scene.getSprites().get(zIndex).get(0);
		assertEquals("SpriteComponent draws the sprite at its actor's position.",
				positionComponent.getPosition(),
				new Vector2(sprite.x, sprite.y));
		assertEquals("SpriteComponent draws at the z-index", zIndex, sprite.zIndex);
	}
	
	@Test
	public void spriteOffset() {
		Vector2 offset = new Vector2(-5, -5);
		Vector2 entityPosition = positionComponent.getPosition();
		spriteComponent.setOffset(offset);
		
		entity.update(0.1f);
		
		Sprite s = scene.getSprites().get(zIndex).get(0);
		assertEquals("SpriteComponent draws sprite offset",
				entityPosition.cpy().add(offset),
				new Vector2(s.x, s.y));
	}
	
	@Test
	public void centreOffset() {
		Vector2 centreOffset = spriteComponent.getCentreOffset(20, 10);
		assertEquals("getCentreOffset returns an offset to centre the sprite.", new Vector2(-10, -5), centreOffset);
	}
	
	@Test
	public void flipped() {
		spriteComponent.setFlipped(true, true);
		assertEquals("SpriteComponent holds flip state", true, spriteComponent.isFlippedX());
		assertEquals("SpriteComponent holds flip state", true, spriteComponent.isFlippedY());
	}
	
	@Test
	public void setColor() {
		spriteComponent.setColor(Color.GREEN);
		spriteComponent.render(0.1f);
		
		Sprite sprite = scene.getSprites().get(zIndex).get(0);
		assertEquals("SpriteComponent draws the sprite with its color.",
				Color.GREEN, sprite.color);
	}
	
	@Test
	public void setTemporaryColor() {
		spriteComponent.setTemporaryColor(Color.GREEN);
		spriteComponent.render(0.1f);
		
		Sprite sprite = scene.getSprites().get(zIndex).get(0);
		assertEquals("SpriteComponent draws the sprite with its color.",
				Color.GREEN, sprite.color);
		
		scene.getSprites().clear();
		spriteComponent.render(0.1f);
		
		sprite = scene.getSprites().get(zIndex).get(0);
		assertEquals("setTemporaryColor causes the color to revert after one frame.",
				Color.WHITE, sprite.color);
	}

}
