package uk.co.samatkins.components.image;

import static org.junit.Assert.*;

import com.badlogic.gdx.Gdx;
import org.junit.Test;

import uk.co.samatkins.components.image.AnimatedSpriteComponent;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimatedSpriteComponentTest {

	/**
	 * Not sure how to test this, as I can't instantiate a Texture without libgdx running.
	 */
	//@Test
	public void animation() {
        FileHandle fh = Gdx.files.internal("assets/goblin.png");
		Texture t = new Texture(fh);
		AnimatedSpriteComponent c = new AnimatedSpriteComponent(
				null, //"null", //new TextureRegion(t),
		8, 8);
		String animationName = "test";
		c.addAnimation(animationName, new int[][] {
				{0,0},
				{0,1},
				{1,0},
				{1,1}
		}, 10, true); // 10 fps = 0.1f per frame
		
		c.playAnimation(animationName, true);
		assertEquals("playAnimation() starts the animation",
				animationName, c.getCurrentAnimationName());
	}
	
	@Test
	public void animationIncrementsFrames() {
//		fail();
	}

}
