package uk.co.samatkins.components.image;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import uk.co.samatkins.Entity;
import uk.co.samatkins.Scene;
import uk.co.samatkins.SceneTest;
import uk.co.samatkins.components.geom.PositionComponent;
import uk.co.samatkins.components.image.TilemapComponent;
import uk.co.samatkins.components.image.TilemapComponent.ITile;
import uk.co.samatkins.exceptions.NoValidPathException;

public class TilemapComponentTest {
	
	private int tilesH, tilesV;
	private TilemapComponent<MockTile> tilemap;
	private Entity entity;
	private Scene<?> scene;
	private Vector2 entityPosition;
	
	public enum MockTile implements ITile {
		A, B, C, D;

		@Override
		public TextureRegion getTextureRegion() {
			return new TextureRegion();
		}

		@Override
		public int getTileWidth() {
			return 10;
		}

		@Override
		public int getTileHeight() {
			return 10;
		}
	};
	
	@Before
	public void setup() {
		tilesH = 10;
		tilesV = 8;
		tilemap = new TilemapComponent<MockTile>(MockTile.class,
					MockTile.A.getTileWidth(), MockTile.A.getTileHeight(),
					tilesH, tilesV);
		
		entity = new Entity();
		entityPosition = new Vector2(100,100);
		entity.addComponent(new PositionComponent(entityPosition));
		entity.addComponent(tilemap);
		
		scene = SceneTest.newScene();
		scene.resize(1000, 1000);
		scene.addEntity(entity);
	}

	@Test
	public void storesAndDrawsTiles() {
		// To start with, all should be null. Just test a centre tile.
		assertNull("Initially, all tiles should be null.", tilemap.getTile(5, 5));
		
		int x = 3, y = 4;
		tilemap.setTile(x, y, MockTile.A);
		assertEquals("setTile() correctly sets a tile at the given position.",
				MockTile.A, tilemap.getTile(x, y));
		
		assertNull("getTile() returns null for invalid coordinates.",
				tilemap.getTile(-1,-1));
		
		tilemap.render(0.1f);
		assertEquals("drawSprite() was called", true, scene.getSpriteCount() > 0);
	}
	
	@Test
	public void storesSolidState() {
		// To start with, nothing is solid.
		assertFalse("Initially, all tiles are non-solid", tilemap.isTileSolid(5,5));
		
		int x = 3, y = 4;
		tilemap.setTileSolid(x, y, true);
		assertTrue("setTileSolid() correctly sets solid state at the given position.",
				tilemap.isTileSolid(x, y));
		
		assertTrue("isTileSolid() returns true for invalid coordinates.",
				tilemap.isTileSolid(-1, -1));
	}
	
	@Test
	public void setRectSolid() {
		int w = 3, h = 4;
		int x1 = 2, x2 = 4,
			y1 = 3, y2 = 6;
		
		tilemap.setRectSolid(x1, y1, w, h, true);
		assertTrue("setRectSolid() correctly sets the lower corner",
				tilemap.isTileSolid(x1,  y1));
		assertTrue("setRectSolid() correctly sets the upper corner",
				tilemap.isTileSolid(x2,  y2));
		assertFalse("setRectSolid() doesn't go bigger than it should",
				tilemap.isTileSolid(x2+1,  y2+1));
	}
	
	@Test
	public void rectContainsSolid() {
		tilemap.setTileSolid(3,3, true);
		
		assertTrue("rectContainsSolid() returns true if at least one tile within the rectangle is solid",
				tilemap.rectContainsSolid(2,2, 3,3));
		assertFalse("rectContainsSolid() returns false when there are no solid tiles within the rectangle",
				tilemap.rectContainsSolid(3,4, 3,3));
	}
	
	@Test
	public void coordinatesToTileCoordinates() {
		Vector2 stageCoordinates = new Vector2(156, 126);
		Vector2 coords = new Vector2(5,2);
		assertEquals("Grid position is correct.", coords, tilemap.getTileCoordinatesAt(stageCoordinates) );
	}
	
	@Test
	public void getPositionOfTile() {
		Vector2 stageCoordinates = new Vector2(156, 126);
		Vector2 coords = new Vector2(150,120);
		assertEquals("Return the position of the bottom-left corner of the tile at the given coordinates",
				coords, tilemap.getPositionOfTile(tilemap.getTileCoordinatesAt(stageCoordinates)) );
	}
	
	@Test
	public void getWidthAndHeight() {
		assertEquals("Tilemap width equals tile width times tiles across",
				100, tilemap.getWidth());
		assertEquals("Tilemap height equals tile height times tiles down",
				80, tilemap.getHeight());
	}
	
	@Test
	public void pathfinding() {
		int startX = 0,
			startY = 0;
		int endX = tilesH-1,
			endY = tilesV-1;
		
		List<Vector2> path = null;
		try {
			path = tilemap.findPath(startX, startY, endX, endY);
			assertNotNull("findPath() must not return null", path);
		} catch (NoValidPathException e) {
			fail("findPath() failed to find a path when there are no obstacles");
		}
		assertEquals("Path starts at the start position", new Vector2(startX, startY), path.get(0));
		assertEquals("Path ends at the end position", new Vector2(endX, endY), path.get(path.size()-1));
		
		tilemap.setRectSolid(1, 0, 1, tilesV, true);

		try {
			tilemap.findPath(startX, startY, endX, endY);
			fail("findPath() must throw a NoValidPathException when a path is impossible.");
		} catch (NoValidPathException e) {
		}
	}

}
