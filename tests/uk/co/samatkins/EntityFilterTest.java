package uk.co.samatkins;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import uk.co.samatkins.geom.Rectangle;

import com.badlogic.gdx.math.Vector2;

public class EntityFilterTest {
	Collection<Entity> entities;
	EntityFilter filter;

	@Before
	public void setup() {
		// Create some entities
		Entity e0 = SceneTest.makeEntity(new Rectangle(95, 195, 10, 10), "thing");
		
		Entity e1 = SceneTest.makeEntity(new Rectangle(95, 195, 10, 10), "otherthing");
		
		Entity e2 = SceneTest.makeEntity(new Rectangle(1000, 1000, 10, 10), "thing");
		e2.addComponent(new MockComponent());
		
		entities = new HashSet<Entity>();
		entities.add(e0);
		entities.add(e1);
		entities.add(e2);
		
		filter = new EntityFilter(entities);
	}
	
	@Test
	public void EntityFilter() {
		assertEquals("Without filtering, getEntities() returns original collection",
				entities, filter.getEntities());
		assertNotSame("EntityFilter clones the collection, so as to not corrupt the Scene.",
				entities, filter.getEntities());
	}
	
	@Test
	public void overlappingPosition() {
		filter.overlappingPosition(new Vector2(101, 201));
		assertEquals("overlappingPosition() removes entities that don't overlap the given position.",
				2, filter.size());
	}
	
	@Test
	public void overlappingShape() {
		filter.overlappingShape(new Rectangle(95, 195, 10, 10));
		assertEquals("overlappingShape() removes entities that don't overlap the given shape.",
				2, filter.size());
	}
	
	@Test
	public void withTag() {
		filter.withTag("thing");
		assertEquals("withTag() removes entities that don't have the given tag.",
				2, filter.size());
	}
	
	@Test
	public void withoutTag() {
		filter.withoutTag("thing");
		assertEquals("withoutTag() removes entities that have the given tag.",
				1, filter.size());
	}
	
	@Test
	public void withComponent() {
		filter.withComponent(MockComponent.class);
		assertEquals("withComponent() removes entities that don't have the given component.",
				1, filter.size());
	}
	
	@Test
	public void first() {
		assertNotNull("first() returns an Entity when there is one to return.", filter.first());

		filter.withTag("Tag that doesn't exist");
		assertNull("first() returns null when there are no entities.", filter.first());
	}

}
