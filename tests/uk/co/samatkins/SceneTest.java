package uk.co.samatkins;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import uk.co.samatkins.components.geom.PositionComponent;
import uk.co.samatkins.geom.Rectangle;
import uk.co.samatkins.geom.Shape;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class SceneTest {
	
	private Scene<MockGame> scene;
	
	/**
	 * Generates a new minimal Scene object
	 * @return
	 */
	public static Scene<MockGame> newScene() {
		return new Scene<MockGame>(new MockGame(), null);
	}

	/**
	 * Generate an Entity with a position and type
	 */
	public static Entity makeEntity(Vector2 position, String type) {
		Entity e = new Entity(type);
		e.addComponent(new PositionComponent(position));
		
		return e;
	}
	
	/**
	 * Generate an Entity with a shape and type
	 */
	public static Entity makeEntity(Shape shape, String type) {
		Entity e = new Entity(type);
		e.addComponent(new PositionComponent(shape));
		
		return e;
	}
	
	@Before
	public void setup() {
		scene = newScene();
	}

	@Test
	public void addEntity() {
		assertEquals("Scene starts with 0 entities.", 0, scene.getEntityCount());
		
		Entity e = new Entity();
		
		Entity returnedE = scene.addEntity(e);
		scene.update(0.1f);
		
		assertEquals("addEntity returns the entity given", e, returnedE);
		assertEquals("Number of entities in Scene increases when one is added.", 1, scene.getEntityCount());
		
		assertNotNull("Added entity must have setScene() called", e.getScene());
		
		scene.addEntity(e);
		scene.update(0.1f);
		assertEquals("Adding the same entity a second time should do nothing.", 1, scene.getEntityCount());
	}
	
	@Test
	public void updateEntities() {
		Entity e = new Entity();
		MockComponent c = new MockComponent();
		e.addComponent(c);
		scene.addEntity(e);
		scene.update(0.1f);
		assertEquals("render() updates all entities.", 1, c.timesUpdated);
	}
	
	@Test
	public void renderingSprites() {
		TextureRegion tr = new TextureRegion();
		Vector2 position = Vector2.Zero;
		int zIndex = 100;
		Color color = new Color(1, 1, 1, 0.5f);
		
		scene.drawSprite(tr, position.x, position.y, zIndex, color);
		assertEquals("drawSprite() adds the sprite to the list", 1, scene.getSpriteCount());
		Sprite sprite = scene.getSprites().get(zIndex).get(0);
		
		assertEquals("drawSprite() uses the TextureRegion", tr, sprite.textureRegion);
		assertEquals("drawSprite() uses the zIndex", zIndex, sprite.zIndex);
		assertEquals("drawSprite() uses the color", color, sprite.color);
		
		scene.draw();
	}
	
	@Test
	public void getEntitiesByType() {
		scene.addEntity(new Entity("type1"));
		scene.addEntity(new Entity("type1"));
		scene.addEntity(new Entity("type1"));
		scene.addEntity(new Entity());
		scene.addEntity(new Entity("other"));
		
		assertEquals("getEntitiesByTag() returns only the entities of the given type",
				3, scene.getEntitiesByTag("type1").size());
		
		assertNotNull("getEntitiesByTag() always returns a valid List",
				scene.getEntitiesByTag("nonexistent"));
		
		assertNotNull("getEntityByTag() returns a single entity",
				scene.getEntityByTag("other"));
	}
	
	@Test
	public void getClosestEntityByType() {
		String type = "type";
		
		Entity entity = makeEntity(new Vector2(100, 100), type);
		scene.addEntity(entity);
		
		Entity near = makeEntity(new Vector2(120, 100), type);
		scene.addEntity(near);

		Entity far = makeEntity(new Vector2(220, 100), type);
		scene.addEntity(far);
		
		Entity wrongType = makeEntity(new Vector2(110, 100), "wrong");
		scene.addEntity(wrongType);
		
		Entity found = scene.getClosestEntity(type, entity);
		assertEquals("getClosestEntity() returns the Entity closest to that given, with the given type, but not the entity itself",
				near, found);
	}
	
	@Test
	public void widgets() {
		MockWidget widget = new MockWidget();
		
		scene.addActor(widget);
		
		assertEquals("Actors get added to the scene.", 1, scene.getActorCount());
		
		float delta = 0.1f;
		scene.update(delta);
		
		assertEquals("Actors that are added to the scene get updated.",
				delta, widget.getDelta(), 0.001f );
		// Cannot test rendering, as spritebatch cannot be initialised.
//		assertEquals("Widgets that are added to the scene get drawn.",
//				1, widget.getTimesDrawn() );
	}
	
	@Test
	public void sceneHasSize() {
		int w = 400,
			h = 300;
		scene.resize(w, h);
		assertEquals("Scene has width", w, scene.getWidth(), 0.01f);
		assertEquals("Scene has height", h, scene.getHeight(), 0.01f);
	}
	
	@Test
	public void resizeListeners() {
		MockResizeListener rl = new MockResizeListener();
		
		int width = 200,
			height = 100;
		scene.addResizeListener(rl);
		scene.resize(width, height);
		assertEquals("resize listeners are called in resize()", width, rl.width);
		assertEquals("resize listeners are called in resize()", height, rl.height);
	}
	
	@Test
	public void loadSounds() {
		/*
		 * I don't think I can actually test sounds, as Gdx needs to exist.
		 */
	}
	
	@Test
	public void constrainCamera() {
		int left = 0,
			right = 1000,
			bottom = 0,
			top = 1000;
		
		Camera camera = scene.getCamera();
		
		scene.constrainCamera(left, right, top, bottom);
		scene.setCameraPosition(left-1, bottom-1);
		
		assertEquals("Camera is constrained on left", left, camera.position.x, 0.01f);
		assertEquals("Camera is constrained on bottom", bottom, camera.position.y, 0.01f);
		
		scene.setCameraPosition(right+1, top+1);
		
		assertEquals("Camera is constrained on right", right, camera.position.x, 0.01f);
		assertEquals("Camera is constrained on top", top, camera.position.y, 0.01f);
		
		scene.moveCamera(20, 20);

		assertEquals("Camera is constrained on right", right, camera.position.x, 0.01f);
		assertEquals("Camera is constrained on top", top, camera.position.y, 0.01f);
	}
	
	@Test
	public void scaling() {
		float scale = 2;
		scene.setScale(scale);
		scene.resize(1000, 2000);
		
		assertEquals("Scene viewport width respects scale", 500, scene.getWidth(), 0.01f);
		assertEquals("Scene viewport height respects scale", 1000, scene.getHeight(), 0.01f);
		
		scene.setScale(0.5f);

		assertEquals("Setting scale resizes scene", 2000, scene.getWidth(), 0.01f);
		assertEquals("Setting scale resizes scene", 4000, scene.getHeight(), 0.01f);
	}
	
	@Test
	public void destroyEntity() {
		Entity e = new Entity();
		scene.addEntity(e);
		scene.update(0.1f);
		
		scene.removeEntity(e);
		scene.update(0.1f);
		
		assertEquals("removeEntity() removes the entity", 0, scene.getEntityCount());
		assertNull("removeEntity() called removed in Entity", e.getScene());
	}
	
	@Test
	public void getEntitiesAtPosition() {
		Vector2 position = new Vector2(100, 200);
		
		Entity wantedEntity = makeEntity(new Rectangle(95, 195, 10, 10), "thing");
		
		Entity e1 = makeEntity(new Rectangle(95, 195, 10, 10), "otherthing");
		
		Entity e2 = makeEntity(new Rectangle(1000, 2000, 10, 10), "thing");
		
		scene.addEntity(wantedEntity);
		scene.addEntity(e1);
		scene.addEntity(e2);
		scene.update(0.1f);
		
		position.add(1, 1);
		
		EntityFilter entities = scene.getEntitiesAtPosition(position);
		assertEquals("getEntitiesAtPosition() returns entities that overlap the given position.", 2, entities.size());
		
		entities = scene.getEntitiesAtPosition(position, "thing");
		assertEquals("getEntitiesAtPosition() when given a tag only return entities with that tag.", 1, entities.size());
		assertTrue("returned entity has the give tag.", entities.getEntities().iterator().next().hasTag("thing"));

		
	}

}
